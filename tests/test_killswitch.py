import os
import unittest

from killswitch import KillSwitch


class TestKillSwitch(unittest.TestCase):
    def setUp(self):
        self.killswitch = KillSwitch()
        self.killswitch.save_firewall_settings()

    def tearDown(self):
        self.killswitch.disable_kill_switch()
        self.killswitch.restore_firewall_settings()

    @staticmethod
    def __ping(host):
        """
        Returns True if host responds to a ping request
        """
        import subprocess, platform

        # Ping parameters as function of OS
        ping_str = "-n 1" if platform.system().lower() == "windows" else "-c 1"
        args = "ping " + " " + ping_str + " " + host
        need_sh = False if platform.system().lower() == "windows" else True

        # Ping
        fnull = open(os.devnull, 'w')
        return subprocess.call(args, shell=need_sh, stdout=fnull) == 0

    def test_kill_switch_is_not_enabled_by_default(self):
        self.assertFalse(self.killswitch.is_kill_switch_enabled(), 'check kill switch disabled')
        self.assertFalse(self.killswitch.is_local_traffic_block_rule_exist(), 'check local traffic block rule is absent')
        self.assertTrue(self.__ping('8.8.8.8'), 'check there is an access to the internet')

    def test_internet_access_disabled_after_kill_switch_enable(self):
        self.assertFalse(self.killswitch.is_kill_switch_enabled(), 'check kill switch disabled')
        self.assertFalse(self.killswitch.is_local_traffic_block_rule_exist(), 'check local traffic block rule is absent')
        self.assertTrue(self.__ping('8.8.8.8'), 'check there is an access to the internet')

        self.assertTrue(self.killswitch.enable_kill_switch(), 'check enable_kill_switch returns True')

        self.assertTrue(self.killswitch.is_kill_switch_enabled(), 'check kill switch enabled')
        self.assertFalse(self.killswitch.is_local_traffic_block_rule_exist(),
                         'check local traffic block rule is absent')
        self.assertFalse(self.__ping('8.8.8.8'), 'check there is no access to the internet')

