@echo off    

REM crate an array of timestamp servers...
set SERVERLIST=(http://timestamp.comodoca.com/authenticode http://timestamp.verisign.com/scripts/timstamp.dll http://timestamp.globalsign.com/scripts/timestamp.dll http://tsa.starfieldtech.com)

REM sign the file...
set signtool="c:\Program Files (x86)\Microsoft SDKs\ClickOnce\SignTool\signtool.exe"
set key="%~dp0\codesign.blackvpn.com.pfx"

%signtool% sign /f %key% /p %BLACKVPN_KEY_PASS% %1

set timestampErrors=0

for /L %%a in (1,1,300) do (

    for %%s in %SERVERLIST% do (

        REM try to timestamp the file. This operation is unreliable and may need to be repeated...
        %signtool% timestamp /t %%s %1

        REM check the return value of the timestamping operation and retry a max of ten times...
        if ERRORLEVEL 0 if not ERRORLEVEL 1 GOTO succeeded

        echo Signing failed. Probably cannot find the timestamp server at %%s
        set /a timestampErrors+=1
    )

    REM wait 2 seconds...
    choice /N /T:2 /D:Y >NUL
)

REM return an error code...
echo ERROR: sign.bat exit code is 1. There were %timestampErrors% timestamping errors.
exit /b 1

:succeeded
REM return a successful code...
echo sign.bat exit code is 0. There were %timestampErrors% timestamping errors.
exit /b 0

:keyisabsent
REM return a successful code...
echo ERROR: Key file is absent. sign.bat exit code is 2. 
exit /b 2
