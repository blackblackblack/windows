# fix for pyinstaller packages app to avoid ReactorAlreadyInstalledError
import sys
if 'twisted.internet.reactor' in sys.modules:
    del sys.modules['twisted.internet.reactor']

#install_twisted_rector must be called before importing the reactor
from kivy.support import install_twisted_reactor, uninstall_twisted_reactor
install_twisted_reactor()

from twisted.internet import reactor, protocol
from twisted.logger._levels import LogLevel
from multiprocessing import Queue
from Queue import Empty
from kivy.clock import Clock
import traceback
import subprocess
# import our own modules
from globals import *

class TelnetClient(protocol.Protocol):
    def connectionMade(self):
        self.factory.app.vpncontrol.on_telnet_connection_ready(self.transport)

    def dataReceived(self, data):
        self.factory.app.vpncontrol.on_vpn_message_received(data)

class TelnetFactory(protocol.ClientFactory):
    protocol = TelnetClient
    def __init__(self, app):
        self.app = app

    def clientConnectionLost(self, conn, reason):
        self.app.log(LOG_WARN, "control connection lost")

    def clientConnectionFailed(self, conn, reason):
        self.app.log(LOG_WARN, "control connection failed")
        self.app.vpncontrol.on_telnet_connection_failed()


class VpnControl:   
    
    app = None       
    
    #_vpnManagementInterface is created by twisted
    _vpnManagementInterface = None
    _vpn_control_message_queue = []
    _last_control_message = ''
    
    _stop_processing_vpn_messages = False
    
    # for testing if we are already connected
    _mode_check_connections = False
    
    _connectionRetryCount = 0
    _connectionRetryCountMax = 10
    
    _vpn_state_refresh_event = None
    
    # speed/data attributes
    _bytecountLastIn = 0
    _bytecountLastOut = 0
    _bytecountInterval = 1
    
    # vpn connection attributes
    _vpn_port = 443
    _vpn_protocol = "udp"
    _vpn_remote = "vpn.blackvpn.nl"
    _vpn_location_id = "NL"
    _vpn_last_location_id = "FR"
    _vpn_stunnel_mode = False
    _vpn_server_ipv4 = ''
    _vpn_server_crypto = 'AES-256-CBC'
    
    # vpn auth details
    _vpn_username = ''
    _vpn_password = ''
    
    _is_disconnected = True
    _is_connecting = False
    _is_connected = False
    
    def __init__(self):
        # App.get_running_app() does not work because the app is not running yet.
        #self.app = App.get_running_app()
        pass
        
    def set_running_app(self, app):
        self.app = app
    
    def log(self, log_level, log_message):
        self.app.log(log_level, log_message)
    
    # Clock scheduled callback from vpn_connect()
    #def reconnect(self, deltatime):
    #    self.vpn_connect()
    
    def create_telnet_connection(self):
        
        self._stop_processing_vpn_messages = False
        #self._vpn_control_message_queue = []
        
        if self._vpn_protocol == 'tcp':
            self.log(LOG_DEBUG, "connecting to TCP management interface...")
            reactor.connectTCP('localhost', 7002, TelnetFactory(self.app))
        else:
            self.log(LOG_DEBUG, "connecting to UDP management interface...")
            reactor.connectTCP('localhost', 7001, TelnetFactory(self.app))

    def on_telnet_connection_failed(self):
        #this happens after a SIGHUP|SIGINT signal
        self._connectionRetryCount += 1
        if self._connectionRetryCount <= self._connectionRetryCountMax:
            self.log(LOG_DEBUG, "retrying control connection...")
            self.create_telnet_connection()
        else:
            self._connectionRetryCount = 0;
            self.log(LOG_DEBUG, "stoped retrying control connection! MaxRetry=" + str(self._connectionRetryCountMax))
            self.restart_vpn_service()

    #callback from EchoClient
    def on_telnet_connection_ready(self, connection):
        self.log(LOG_DEBUG, "connected OK to the management interface")
        self._vpnManagementInterface = connection
        self._connectionRetryCount = 0
        
        if self._mode_check_connections  == False:
            self.set_vpn_status(VPN_STATUS_CONNECTED_VPN_MTM)
            self.send_vpn_control_message("log on")
            
        self.send_vpn_control_message("state")
        
        
        #-------------------------------------------------------------------
        # after this on_vpn_message_received() will take care of the rest
        #-------------------------------------------------------------------
        
    def send_vpn_control_message(self, message):        
        if message.find('password') > -1:
            self.log(LOG_DEBUG, 'Sending VPN control msg: password "Auth" ?????????')
        else:
            self.log(LOG_DEBUG, "Sending VPN control msg: " + message)
        
        if self._vpnManagementInterface is None:
            self.log(LOG_WARN, 'Control Interface is dead! Message NOT sent')
        else:
            try:
                self._last_control_message = message
                self._vpnManagementInterface.write(str(message) + '\n')
            except:
                self.log(LOG_ERROR, "Error sending VPN control message: " + str(message))
            
    def disconnect_management_interface(self, deltatime):
        
        # cancel the event to refresh vpn state
        if self._vpn_state_refresh_event is not None:
            self._vpn_state_refresh_event.cancel()
            print "CANCELLED EVENT FOR STATE CHECK in disconnect_management_interface()"
            self._vpn_state_refresh_event = None
        
        if self._vpnManagementInterface is not None:
            self.log(LOG_INFO, 'disconnecting from Management Interface...')
            self._vpnManagementInterface.loseConnection()
        else:
            self.log(LOG_WARN, 'Control Interface is dead! Could not disconnect')
            
    def set_vpn_status(self, vpn_status):
        self.log(LOG_DEBUG, 'vpn connect status is: ' + str(vpn_status) + '%')
        
        # Save for laterz
        if vpn_status == VPN_STATUS_DISCONNECTED:
            self.set_status_disconnected()
        elif vpn_status == VPN_STATUS_CONNECTED:
            self.set_status_connected()
        else:
            self.set_status_connecting()
            
        # callback to the main app
        self.app.update_vpn_status(vpn_status)

    def vpn_connect(self, *args):
        self.log(LOG_DEBUG, 'in vpncontrol.vpn_connect()')
        #self.create_telnet_connection()
        
        # Just connect if there is no connection
        if self._vpnManagementInterface is None:
            self.create_telnet_connection()
           
        # Connect is we're disconnected 
        elif self._is_disconnected == True:
            self.create_telnet_connection()
            
        elif self._is_connected == True:
            # are we trying to connect to the VPN we're already conencted to?
            if self._vpn_last_location_id == self._vpn_location_id:
                self.log(LOG_WARN, 'You tried to connect to ' + str(self._vpn_location_id) + ' but you are already connected to ' + str(self._vpn_last_location_id))

            # first disconnect the current connection the reconnect
            self.vpn_disconnect()
            #TODO adjust timeout here
            self.log(LOG_WARN, "vpnconntrol.vpn_connect(): Running RECONNECT")
            Clock.schedule_once(self.vpn_connect, 0.1)

        self.log(LOG_DEBUG, 'out vpncontrol.vpn_connect()')

    def vpn_disconnect(self, *args):
        #kill connection with SIGHUP
        try:
            if self._vpnManagementInterface is not None:
                
                self._stop_processing_vpn_messages = True
                self.send_vpn_control_message("signal SIGHUP")
                
                # need a slight delay before disconnecting otherwise SIGHUP is not processed
                Clock.schedule_once(self.disconnect_management_interface, 0.3)
                
                self.set_vpn_status(VPN_STATUS_DISCONNECTED)
            else:
                self.log(LOG_WARN, "self._vpnManagementInterface is None! Could not disconnect VPN")
        except KeyboardInterrupt:
            raise
        except:
            e = sys.exc_info()[0]
            stacktrace =  traceback.format_exc()
            self.log(LOG_ERROR, str(e))
            self.log(LOG_ERROR, str(stacktrace))
            
    def check_connections(self, timedelta):
        self.log(LOG_DEBUG, "Checking if we are already connected with " + self.get_vpn_protocol().upper() + ":" + str(self.get_vpn_port()))
        self._mode_check_connections = True
        self.create_telnet_connection()
            
            
    #-------------------------------------------------------------------
    # stuff happens here - respond to messages from OpenVPN
    #-------------------------------------------------------------------
            
    def on_vpn_message_received(self, data):
        
        # data can be many lines so process each line one at a time...
        message_queue = data.split('\n\r')
        for original_msg in message_queue:
            
            # strip whitespace from end
            # strangely this gets added by the data.split('\n\r')
            msg = original_msg.strip('\t\n\r')
            
            #skip if no message after whitespace gone
            if len(msg) == 0:
                pass
            
            #log message (unless its bytecount)
            if msg.find('BYTECOUNT:') == -1:
                self.log(LOG_VPN, msg)
                
            # if we're disconnecting then stop processing messages (or it gets complicated)
            if self._stop_processing_vpn_messages == True:
                self.log(LOG_WARN, "Stopped processing messages. Message will be ignored: " + str(data))
                return
            
                
            ############################################
            # Alerts & Errors
            ############################################
        
            # sometimes we get in this state between disconnecting and reconnecting
            if msg.find('RECONNECTING,SIGHUP') > -1 and self._mode_check_connections == False:
                self.log(LOG_DEBUG, "Processing VPN message: 'RECONNECTING,SIGHUP'")
                if self._last_control_message == 'hold release':
                    self.log(LOG_INFO, "Not sending 'signal SIGHUP' since we just sent 'hold release'")
                else:
                    self.log(LOG_INFO, "Not sending 'signal SIGHUP' - ignoring ##### test #####'")
                    #self.send_vpn_control_message("signal SIGHUP")
                    pass
           
            #elif msg.find('RECONNECTING,init_instance') > -1 and self._mode_check_connections == False:
            elif msg.find('RECONNECTING,init_instance') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'RECONNECTING,init_instance'")
                self.send_vpn_control_message("signal SIGUSR1")
                pass
                
            #elif msg.find('RECONNECTING,connection_reset') > -1 and self._mode_check_connections == False:
            elif msg.find('RECONNECTING,connection_reset') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'RECONNECTING,connection_reset'")
                self.send_vpn_control_message("signal SIGUSR1")
                pass
                
            elif msg.find('AUTH_FAILED') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'AUTH_FAILED'")
                self.app.update_notification_alerts('red', 'Login Failed\nPlease check your username + password.')
                self._stop_processing_vpn_messages = True
                self.vpn_disconnect()
                pass
                
            elif msg.find('FATAL:ERROR: could not read Auth') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'FATAL:ERROR: could not read Auth'")
                self.app.update_notification_alerts('red', 'Login Failed\nPlease check your username + password.')
                self._stop_processing_vpn_messages = True
                self.vpn_disconnect()
                pass
                
                #self.send_vpn_control_message('signal SIGUSR1')
                #self.set_vpn_status(VPN_STATUS_DISCONNECTED)
                #Clock.schedule_once(self.disconnect_management_interface,0)
            
            elif msg.find('RECONNECTING,auth-failure') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'RECONNECTING,auth-failure'")
                self.app.update_notification_alerts('red', 'Login Failed\nPlease check your username + password.')
                self.send_vpn_control_message("signal SIGUSR1")
                pass
                #self._stop_processing_vpn_messages = True
                #self.vpn_disconnect()
            
            elif msg.find('RECONNECTING,tls-error') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'RECONNECTING,tls-error'")
                self.send_vpn_control_message("hold release")
                pass
                #self._stop_processing_vpn_messages = True
                #self.vpn_disconnect()

                
            ############################################
            # VPN Control
            ############################################

            if msg.find('CONNECTED,SUCCESS') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'CONNECTED,SUCCESS'")
                
                if self.status_is_connected() == True:
                    self.log(LOG_DEBUG, "Ignoring 'CONNECTED,SUCCESS' since we're already connected")
                    pass
                else:
                    # get the server IPv4 address from the message which looks like:
                    # >LOG:1510328244,D,MANAGEMENT: CMD 'state' 1510328243,CONNECTED,SUCCESS,172.16.203.74,91.109.247.158,443,,,fd69:2205:1941:1::1
                    connection_info = msg.split(',')
                    self._vpn_server_ipv4 = connection_info[6]
                    self.set_vpn_status(VPN_STATUS_CONNECTED)
                    self.send_vpn_control_message("bytecount " + str(self._bytecountInterval))
                    if self._mode_check_connections == True:
                        self._mode_check_connections = False
                    pass

            elif msg.find('WAIT,,,,') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'WAIT,,,,' - (skipping all messages)")
                pass
                

            elif msg.find('>PASSWORD:Need') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: '>PASSWORD:Need'")
                #if self.ids.switch.active == True:
                self.send_vpn_control_message("username \"Auth\" " + self._vpn_username)
                self.send_vpn_control_message("password \"Auth\" " + self._vpn_password)
                Clock.schedule_once(self.send_vpn_control_state_message, 0)
                return
        
            elif msg.find('Restart pause') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'Restart pause'")
                self.set_vpn_status(VPN_STATUS_RESTART_PAUSE)
                return
        
            elif msg.find('>REMOTE:') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: '>REMOTE:'")
                self.send_vpn_control_message("remote MOD " + self._vpn_remote + " " + str(self._vpn_port))
                self.set_vpn_status(VPN_STATUS_REMOTE_MOD)
                return
            
            #if msg.find('>SUCCESS: hold release succeeded'):
            #    self.send_vpn_control_message("remote MOD " + self._vpn_remote + " " + str(self._vpn_port))
            
            elif msg.find('TLS: Initial packet from') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'TLS: Initial packet from'")
                self.set_vpn_status(VPN_STATUS_INITIAL_PACKET)
            
            elif msg.find('PUSH_REQUEST') > -1:
                self.log(LOG_DEBUG, "Processing VPN messages: 'PUSH_REQUEST'")
                self.set_vpn_status(VPN_STATUS_PUSH_REQUEST)
            
            elif msg.find('PUSH_REPLY') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'PUSH_REPLY'")
                self.set_vpn_status(VPN_STATUS_VPN_REPLY)
                
            elif msg.find('ROUTE_GATEWAY') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'ROUTE_GATEWAY'")
                self.set_vpn_status(VPN_STATUS_ROUTE_GATEWAY)
                
            elif msg.find('do_ifconfig') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'do_ifconfig'")
                self.set_vpn_status(VPN_STATUS_DO_IFCONFIG)
            
            elif msg.find('Block_DNS: Added block filters') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'Block_DNS: Added block filters'")
                self.set_vpn_status(VPN_STATUS_BLOCK_DNS)
                
            elif msg.find('Route addition via IPAPI') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'Route addition via IPAPI'")
                self.set_vpn_status(VPN_STATUS_ROUTE_ADDITION)
            
            elif msg.find('Initialization Sequence Completed') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: 'Initialization Sequence Completed'")
                self.send_vpn_control_message("state")
                self.set_vpn_status(VPN_STATUS_INIT_COMPLETE)
                
            # this happens after control signal 
            elif msg.find('>HOLD:Waiting for hold release:') > -1:
                self.log(LOG_DEBUG, "Processing VPN message: '>HOLD:Waiting for hold release:*'")
                self.log(LOG_DEBUG, "######## HOLD RELEASE - 1 -########")
                
                if data.find('>PASSWORD:Need') > -1:
                    self.log(LOG_DEBUG, "No response to >HOLD:Waiting because we need to respond to >PASSWORD:Need instead.")
                    pass
                    
                elif self._mode_check_connections == False:
                    if self._last_control_message == 'hold release':
                        self.log(LOG_DEBUG, "######## HOLD RELEASE - 2 -########")
                        self.send_vpn_control_message("signal SIGUSR1")
                        Clock.schedule_once(self.send_vpn_control_state_message, 3)
                        pass
                    else:
                        self.log(LOG_DEBUG, "######## HOLD RELEASE - 3 -########")
                        self.send_vpn_control_message("hold release")
                        self.set_vpn_status(VPN_STATUS_HOLD_RELEASE)
                        pass
                elif self._mode_check_connections == True:
                    self.log(LOG_DEBUG, "######## HOLD RELEASE - 4 -########")
                    if self.status_is_disconnected() == True:
                        self.log(LOG_DEBUG, "NOT already connected.")
                        self._mode_check_connections = False
                        Clock.schedule_once(self.disconnect_management_interface, 0)
                        
                        # now we have verified that the VPN is not already connected with UDP or TCP
                        # do auto-connect (depending on user setting)
                        Clock.schedule_once(self.app.do_auto_connect, 0.5)
                        return
                pass
            
                
            #if msg.find('process restarting') > -1:
            #    self.set_vpn_status(VPN_STATUS_DISCONNECTED)
            
            elif msg.find('BYTECOUNT') > -1:
                try:
                    byteCountText, bytesTotal = msg.split(':')
                    bytesIn, bytesOut = bytesTotal.split(',')
                
                    if self._bytecountLastIn > 0:
                        bytecountIn = (float(bytesIn) - float(self._bytecountLastIn))/(1024 * self._bytecountInterval)            
                        bytesInGB = (float(bytesIn)/(1024 * 1024 * 1024))
                        #speed_down = "%0.2f Kbps\n[%0.2f]" % (bytecountIn, bytesInGB)
                        speed_down = "%0.2f\nKbps" % (bytecountIn)
                        self.app.update_label_vpn_speed_down(speed_down)
                    if self._bytecountLastOut > 0:   
                        bytecountOut = (float(bytesOut) - float(self._bytecountLastOut))/(1024 * self._bytecountInterval)
                        bytesOutGB = (float(bytesOut)/(1024 * 1024 * 1024))
                        #speed_up = "%0.2f Kbps\n[%0.2f]" % (bytecountOut, bytesOutGB)
                        speed_up = "%0.2f\nKbps" % (bytecountOut)
                        self.app.update_label_vpn_speed_up(speed_up)

                    #store current data for later
                    self._bytecountLastIn = int(bytesIn)
                    self._bytecountLastOut = int(bytesOut)
                    return
                except:
                    pass
                    
                    
    def send_vpn_control_state_message(self, deltatime):
        if self.status_is_disconnected() == True:
            self.log(LOG_INFO, "Not sending 'state' message because we're not connected.")
        else:
            self.send_vpn_control_message("state")
        
    def set_vpn_location(self, remote, location_id):
        self._vpn_remote = remote
        self._vpn_location_id = location_id
        
    def set_vpn_username(self, username):
        self._vpn_username = username
        
    def set_vpn_password(self, password):
        self._vpn_password = password
        
    def set_vpn_port(self, port):
        self._vpn_port = port
        
        if int(port) > 20000:
            self._vpn_stunnel_mode = True
            self._vpn_remote = STUNNEL_LOCALHOST
        else:
            self._vpn_stunnel_mode = False
            
    def set_vpn_stunnel_ports(self, ports_list):
        if ports_list is not None and len(ports_list) > 0:
            self._stunnel_ports_list = ports_list
            self.set_vpn_port(ports_list[0])
        
    def set_vpn_protocol(self, protocol):
        self._vpn_protocol = protocol
        
        if protocol == 'udp':
            self._vpn_stunnel_mode = False
        
    def set_vpn_crypto(self, crypto):
        self._vpn_server_crypto = crypto

    def set_status_disconnected(self):
        if self._is_disconnected != True:
            self.log(LOG_DEBUG, "Setting internal status to: DISCONNECTED")
        
        self._is_disconnected = True
        self._is_connecting = False
        self._is_connected = False
        
        self._bytecountLastIn = 0
        self._bytecountLastOut = 0
        
        #TODO: Keep this?
        if self._vpn_state_refresh_event is not None:
            self._vpn_state_refresh_event.cancel()
            self.log(LOG_DEBUG, "CANCELLED EVENT FOR STATE CHECK in set_status_disconnected()")
        
    def set_status_connecting(self):
        if self._is_connecting != True:
            self.log(LOG_DEBUG, "Setting internal status to: CONNECTING")
            
        self._is_disconnected = False
        self._is_connecting = True
        self._is_connected = False
        
        #TODO: Keep this?
        if self._vpn_state_refresh_event is None:
            self._vpn_state_refresh_event = Clock.schedule_interval(self.send_vpn_control_state_message, 15)
        else:
            pass
        
        
    def set_status_connected(self):
        if self._is_connected != True:
            self.log(LOG_DEBUG, "Setting internal status to: CONNECTED")
            
        self._is_disconnected = False
        self._is_connecting = False
        self._is_connected = True
        
        #TODO: Keep this?
        if self._vpn_state_refresh_event is not None:
            self._vpn_state_refresh_event.cancel()
            self.log(LOG_DEBUG, "CANCELLED EVENT FOR STATE CHECK in set_status_connected()")
        
        
    def status_is_disconnected(self):
        return self._is_disconnected
        
    def status_is_connecting(self):
        return self._is_connecting
        
    def status_is_connected(self):
        return self._is_connected
        
    def get_vpn_port(self):
        return self._vpn_port
    
    def get_vpn_protocol(self):
        return self._vpn_protocol
        
    def get_vpn_remote(self):
        return self._vpn_remote
        
    def get_vpn_location_id(self):
        return self._vpn_location_id
        
    def get_vpn_stunnel_mode(self):
        return self._vpn_stunnel_mode
        
    def get_vpn_server_ipv4(self):
        return self._vpn_server_ipv4
        
    def restart_vpn_service(self):
        try:
            self.log(LOG_WARN, "Stopping the OpenVPN service...")
            #os.system('net.exe stop "OpenVPNService"')
            stop_output = subprocess.check_output(['net.exe', 'stop', 'OpenVPNService'])
            self.log(LOG_INFO, "result was: " + stop_output)
            
            self.set_vpn_status(VPN_STATUS_DISCONNECTED)
            
            self.log(LOG_WARN, "Starting the OpenVPN service...")
            #os.system('net.exe start "OpenVPNService"')
            start_output = subprocess.check_output(['net.exe', 'start', 'OpenVPNService'])
            self.log(LOG_INFO, "result was: " + start_output)
            self.log(LOG_WARN, "OpenVPN service restarted.")
            
            self.app.restart_vpn_service_suceeded()
        except:
            e = sys.exc_info()
            self.log(LOG_WARN, "Error stopping OpenVPN service: " + str(e))
            for msg in e:
                self.log(LOG_WARN, str(msg))
            self.app.restart_vpn_service_failed()
        

