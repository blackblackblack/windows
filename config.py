import keyring

from kivy.config import Config, ConfigParser
from kivy.event import EventDispatcher
from kivy.properties import ConfigParserProperty, StringProperty


class ConfigInitializer(EventDispatcher):
    config_name = "blackvpn"
    config_file = "blackvpn.ini"
    config_section_user = "user"

    # vpn connection attributes with default values
    vpn_protocol = ConfigParserProperty("udp", config_section_user, 'vpn_protocol', config_name)
    vpn_port = ConfigParserProperty(443, config_section_user, 'vpn_port', config_name, val_type=int)
    vpn_remote = ConfigParserProperty("vpn.blackvpn.fr", config_section_user, 'vpn_remote', config_name)
    vpn_location_id = ConfigParserProperty("FR", config_section_user, 'vpn_location_id', config_name)
    vpn_stunnel_mode = ConfigParserProperty(False, config_section_user, 'vpn_stunnel_mode', config_name, val_type=bool)

    # vpn auth details
    vpn_username = StringProperty('b0192885')
    vpn_password = StringProperty('uYjz6Hv3Q')

    # other user settings
    save_auth = ConfigParserProperty(False, config_section_user, 'save_auth', config_name, val_type=bool)

    def __init__(self, **kwargs):
        # file config initialze
        self.cfg = ConfigParser(name=self.config_name)
        self.cfg.read(self.config_file)
        self.cfg.setdefaults(self.config_section_user,
                                {
                                    'vpn_protocol': self.vpn_protocol,
                                    'vpn_port': self.vpn_port,
                                    'vpn_remote': self.vpn_remote,
                                    'vpn_location_id': self.vpn_location_id,
                                    'vpn_stunnel_mode': self.vpn_stunnel_mode,
                                    'save_auth': self.save_auth
                                })
        # we need this write in case of cfg file does not exist, so create it
        self.cfg.write()

        # keyring values restore
        username = keyring.get_password(self.config_name, 'vpn_username')
        if username:
            self.vpn_username = username
        password = keyring.get_password(self.config_name, 'vpn_password')
        if password:
            self.vpn_password = password

        # initialize it after keyring restore to subscribe to "on_<property>" events
        super(ConfigInitializer, self).__init__(**kwargs)

    def on_vpn_username(self, _, value):
        print "vpn_username new value ({}) save to keyring".format(value)
        if self.save_auth:
            keyring.set_password(self.config_name, 'vpn_username', value)
        else:
            keyring.delete_password(self.config_name, 'vpn_username')

    def on_vpn_password(self, _, value):
        print "vpn_password new value save to keyring".format(value)
        if self.save_auth:
            keyring.set_password(self.config_name, 'vpn_password', value)
        else:
            keyring.delete_password(self.config_name, 'vpn_password')


Config = ConfigInitializer()
