import threading
import os
import subprocess
from Queue import Queue

# import our own modules
from globals import *
from funcs import *


class KillSwitch:

    def __init__(self, logger):
        self.log = logger

        self.killswitch_status_updating = Event()
        self.killswitch_status_updated = Event()

        self.firewall_backup_filepath = os.path.join(get_app_path(), FIREWALL_BACKUP_SETTINGS_FILENAME)

        # calling all methods async, but keep calls order
        self.last_method_called = None
        self.queue = Queue()
        t = threading.Thread(target=self.worker)
        t.daemon = True
        t.start()

    def worker(self):
        func = ""
        while True:
            try:
                item = self.queue.get()
                func = getattr(self, "_{}".format(item), None)
                if func is None:
                    func = self.__default

                # skip method call if it is called more than once
                if self.last_method_called != func:
                    result = func()
                    self.log(LOG_INFO, "Call {} result: {}".format(item, result))
                    self.last_method_called = func
                self.queue.task_done()
            except Exception as ex:
                self.log(LOG_ERROR, "killswitch '{}' method call error: {}".format(func, ex.message))

    def enable_kill_switch(self):
        self.queue.put("enable_kill_switch")

    def disable_kill_switch(self):
        self.queue.put("disable_kill_switch")

    def allow_local_traffic(self):
        self.queue.put("allow_local_traffic")

    def block_local_traffic(self):
        self.queue.put("block_local_traffic")

    def save_firewall_settings(self):
        self.queue.put("save_firewall_settings")

    def restore_firewall_settings(self):
        self.queue.put("restore_firewall_settings")

    def __default(self):
        self.log(LOG_ERROR, "There is no such a function")
        return True

    def _enable_kill_switch(self):
        """
        Enable killswitch:
        1) Make sure backup file exist, so we can restore original firewall settings
        2) enable windows firewall if if is Off
        3) block all inbound and outbound traffic
        4) allow OpenVPN traffic to be able to connect to VPN
        5) allow traffic throw VPN localIP
        """
        if self.is_kill_switch_enabled():
            return True
        if not os.path.exists(self.firewall_backup_filepath):
            self.log(LOG_WARN, "There is no firewall settings backup yet, so skip killswitch, "
                               "as it would not be possible to rollback it.")
            return False
        self.log(LOG_INFO, "Adding firewall rules")
        stunnel_dir = get_stunnel_dir()
        blackvpn_dir = r'C:\Program Files\BlackVPN'
        try:
            self.killswitch_status_updating()
            return self.__run_netsh(r'set allprofiles state on') and \
                   self.__run_netsh(r'set allprofiles firewallpolicy blockinbound,blockoutbound') and \
                   self.__run_netsh(r'firewall add rule name="{}" protocol=any dir=OUT action=allow '
                                    r'program="{}\openvpn\bin\openvpn.exe"'
                                    .format(FIREWALL_ENABLE_OPENVPN_RULE_NAME, blackvpn_dir)) and \
                   self.__run_netsh(r'firewall add rule name="{}-openssl" protocol=any dir=OUT action=allow '
                                    r'program="{}\openvpn\bin\openssl.exe"'
                                    .format(FIREWALL_ENABLE_OPENVPN_RULE_NAME, blackvpn_dir)) and \
                   self.__run_netsh(r'firewall add rule name="{}" protocol=any dir=OUT action=allow '
                                    r'program="{}\bin\stunnel.exe"'
                                    .format(FIREWALL_ENABLE_STUNNEL_RULE_NAME, stunnel_dir)) and \
                   self.__run_netsh(r'firewall add rule name="{}-tstunnel" protocol=any dir=OUT action=allow '
                                    r'program="{}\bin\tstunnel.exe"'
                                    .format(FIREWALL_ENABLE_STUNNEL_RULE_NAME, stunnel_dir)) and \
                   self.__run_netsh(r'firewall add rule name="{}-openssl" protocol=any dir=OUT action=allow '
                                    r'program="{}\bin\openssl.exe"'
                                    .format(FIREWALL_ENABLE_STUNNEL_RULE_NAME, stunnel_dir)) and \
                   self.__run_netsh(r'firewall add rule name="{}" protocol=any dir=OUT action=allow localip={}'
                                    .format(FIREWALL_ENABLE_VPN_RULE_NAME, FIREWALL_VPN_LOCAL_IPS))
        finally:
            self.killswitch_status_updated()

    def _disable_kill_switch(self):
        if not self.is_kill_switch_enabled():
            return True
        return self._restore_firewall_settings()

    def _block_local_traffic(self):
        if self.is_local_traffic_block_rule_exist():
            return self.__run_netsh('firewall delete rule "{}"'.format(FIREWALL_ENABLE_LOCAL_RULE_NAME))
        return True

    def _allow_local_traffic(self):
        if not self.is_kill_switch_enabled():
            return False
        if not self.is_local_traffic_block_rule_exist():
            cmd = 'firewall add rule name="{}" protocol=any dir=OUT action=allow remoteip=localsubnet'
            return self.__run_netsh(cmd.format(FIREWALL_ENABLE_LOCAL_RULE_NAME))
        return True

    def _save_firewall_settings(self):
        """ 
        Calling this method is mandatory to be able to disable killswitch.
        """
        if self.is_kill_switch_enabled():
            # the method must be called when there is no killswitch activated yet
            self.log(LOG_WARN, "Killswitch is activated already, so skip firewall settings saving.")
            return False
        if os.path.exists(self.firewall_backup_filepath):
            os.remove(self.firewall_backup_filepath)
        return self.__run_netsh('export "{}"'.format(self.firewall_backup_filepath))

    def _restore_firewall_settings(self):
        try:
            self.killswitch_status_updating()
            if os.path.exists(self.firewall_backup_filepath):
                self.log(LOG_INFO, "Importing original firewall rules")
                return self.__run_netsh('import "{}"'.format(self.firewall_backup_filepath))
            # Here there is a problem - there is no file to restore firewall settings,
            # but we have to disable killswitch somehow.
            # It would rarely happen because of we verify we have restore
            # file before kill switch enabled, but who knows...
            self.log(LOG_ERROR, "There is no file to restore firewall settings, "
                                "so try to reset firewall settings to defaults.")
            return self.__run_netsh('reset')
        finally:
            self.killswitch_status_updated()

    def is_kill_switch_enabled(self):
        return self.__is_rule_exist(FIREWALL_ENABLE_OPENVPN_RULE_NAME)

    def is_local_traffic_block_rule_exist(self):
        return self.__is_rule_exist(FIREWALL_ENABLE_LOCAL_RULE_NAME)
       
    def is_system_ready_for_firewall_settings_backup(self):
        if self.is_kill_switch_enabled():
            return False
        if not self.__ping('8.8.8.8'):
            return False
        return True
       
    def __is_rule_exist(self, name):
        try:
            rule_output = self.__run_cmd('netsh advfirewall firewall show rule name="{}"'.format(name))
            return True
        except subprocess.CalledProcessError:
            return False

    def __run_netsh(self, args, print_cmd_and_error=True):
        output = ""
        try:
            if print_cmd_and_error:
                cmd = 'netsh advfirewall {}'.format(args)
            self.log(LOG_INFO, 'killswitch CMD: {}'.format(cmd))
            output = self.__run_cmd(cmd)
        except subprocess.CalledProcessError as ex:
            if print_cmd_and_error:
                self.log(LOG_ERROR, ex)
                self.log(LOG_ERROR, 'Code: {}. Output: {}'
                         .format(ex.returncode, ex.output.strip()[len('Active code page: 437') + 2:]))
        if not output.endswith('Ok.'):
            return False
        return True

    @staticmethod
    def __run_cmd(cmd):
        """
        Use 437 (english) encoding here to make sure we always have
        the same response from the command for different user's locales.
        """
        output = subprocess.check_output('chcp 437 & {}'.format(cmd), stderr=subprocess.STDOUT, shell=True)
        return output.strip()[len('Active code page: 437') + 2:]
        
    @staticmethod
    def __ping(host):
        """
        Returns True if host responds to a ping request
        """
        import platform

        # Ping parameters as function of OS
        ping_str = "-n 1" if platform.system().lower() == "windows" else "-c 1"
        args = "ping " + " " + ping_str + " " + host
        need_sh = False if platform.system().lower() == "windows" else True

        # Ping
        fnull = open(os.devnull, 'w')
        return subprocess.call(args, shell=need_sh, stdout=fnull) == 0
