#LOG STATUS STUFF
LOG_DEBUG = 1
LOG_INFO = 2
LOG_WARN = 3
LOG_ERROR = 4
LOG_CRITICAL = 5
LOG_VPN = 6

# VPN STATUS STUFF
VPN_STATUS_DISCONNECTED = 0
VPN_STATUS_CONNECTED_VPN_MTM = 5
VPN_STATUS_HOLD_RELEASE = 10
VPN_STATUS_RESTART_PAUSE = 20
VPN_STATUS_REMOTE_MOD = 25
VPN_STATUS_INITIAL_PACKET = 30
VPN_STATUS_PUSH_REQUEST = 40
VPN_STATUS_VPN_REPLY = 50
VPN_STATUS_ROUTE_GATEWAY = 60
VPN_STATUS_DO_IFCONFIG = 70
VPN_STATUS_BLOCK_DNS = 80
VPN_STATUS_ROUTE_ADDITION = 90
VPN_STATUS_INIT_COMPLETE = 95
VPN_STATUS_CONNECTED = 100

SUPPORT_URL = 'https://support-api.blackvpn.com/tickets.json'
SUPPORT_API_KEY = '06174B154C2DA1791DC573142AD8C300'
SUPPORT_ENABLE_TIMEOUT = 60

STUNNEL_LOCALHOST = '127.0.0.1'

APP_VERSION = 0.987
APP_UPDATES_AUTO_CHECK_DELAY = 90
APP_UPDATES_URL = 'https://www.blackvpn.com/content/uploads/app/app-updates.json'
APP_UPDATES_ZIP_FILENAME = 'blackvpn-updates.zip'
APP_UPDATES_EXE_FILENAME = 'blackvpn-updates.exe'

CRASH_REPORT_FILENAME = 'app-crash-report.txt'
DEBUG_REPORT_FILENAME = 'app-debug.txt'

FIREWALL_BACKUP_SETTINGS_FILENAME = 'firewall_default_settings.wfw'
FIREWALL_VPN_LOCAL_IPS = '172.1.1.1/8'
FIREWALL_ENABLE_VPN_RULE_NAME = 'BlackVPN-EnableVPN'
FIREWALL_ENABLE_OPENVPN_RULE_NAME = 'BlackVPN-EnableOpenVPN'
FIREWALL_ENABLE_STUNNEL_RULE_NAME = 'BlackVPN-EnableStunnel'
FIREWALL_ENABLE_LOCAL_RULE_NAME = 'BlackVPN-EnableLocal'

VPN_GLOBAL_LIST = {
    'AU': {'name': 'Australia', 'remote': 'australia.vpn.blackvpn.com', 'background': 'data/backgrounds/AU.png', 'p2p': True, 'stunnel_ports': [23462, 23469]},
    'BR': {'name': 'Brazil', 'remote': 'brazil.vpn.blackvpn.com', 'background': 'data/backgrounds/BR.png', 'p2p': True, 'stunnel_ports': [23464]},
    'CA': {'name': 'Canada', 'remote': 'ca.vpn.blackvpn.com', 'background': 'data/backgrounds/CA.png', 'p2p': True, 'stunnel_ports': [23440, 23471]},
    'CZ': {'name': 'Czechia', 'remote': 'czech.vpn.blackvpn.com', 'background': 'data/backgrounds/CZ.png', 'p2p': True, 'stunnel_ports': [23453]},
    'EE': {'name': 'Estonia', 'remote': 'vpn.blackvpn.ee', 'background': 'data/backgrounds/EE.png', 'p2p': True, 'stunnel_ports': [23443]},
    'FR': {'name': 'France', 'remote': 'vpn.blackvpn.fr', 'background': 'data/backgrounds/FR.png', 'p2p': True, 'stunnel_ports': [23467]},
    'DE': {'name': 'Germany', 'remote': 'vpn.blackvpn.de', 'background': 'data/backgrounds/DE.png', 'p2p': True, 'stunnel_ports': [23468]},
    'JP': {'name': 'Japan', 'remote': 'japan.vpn.blackvpn.com', 'background': 'data/backgrounds/JP.png', 'p2p': True, 'stunnel_ports': [23441]},
    'LT': {'name': 'Lithuania', 'remote': 'vpn.blackvpn.lt', 'background': 'data/backgrounds/LT.png', 'p2p': True, 'stunnel_ports': [23444]},
    'LU': {'name': 'Luxembourg', 'remote': 'vpn.blackvpn.lu', 'background': 'data/backgrounds/LU.png', 'p2p': True, 'stunnel_ports': [23446, 23447]},
    'NL': {'name': 'Netherlands', 'remote': 'vpn.blackvpn.nl', 'background': 'data/backgrounds/NL.png', 'p2p': True, 'stunnel_ports': [23472, 23473, 23474]},
    'NO': {'name': 'Norway', 'remote': 'norway.vpn.blackvpn.com', 'background': 'data/backgrounds/NO.png', 'p2p': True, 'stunnel_ports': [23445]},
    'RO': {'name': 'Romania', 'remote': 'vpn.blackvpn.ro', 'background': 'data/backgrounds/RO.png', 'p2p': True, 'stunnel_ports': [23463]},
    'ES': {'name': 'Spain', 'remote': 'spain.vpn.blackvpn.com', 'background': 'data/backgrounds/ES.png', 'p2p': True, 'stunnel_ports': [23469]},
    'CH': {'name': 'Switzerland', 'remote': 'vpn.blackvpn.ch', 'background': 'data/backgrounds/CH.png', 'p2p': True, 'stunnel_ports': [23475, 23476]},
    'UA': {'name': 'Ukraine', 'remote': 'vpn.blackvpn.com.ua', 'background': 'data/backgrounds/UA.png', 'p2p': True, 'stunnel_ports': [23454, 23455]},
    'UK': {'name': 'UK London', 'remote': 'vpn.blackvpn.co.uk', 'background': 'data/backgrounds/UK.png', 'p2p': False, 'stunnel_ports': [23458, 23457]},
    'USA_C': {'name': 'USA Central', 'remote': 'central.vpn.blackvpn.com', 'background': 'data/backgrounds/USA_C.png', 'p2p': False, 'stunnel_ports': [23456]},
    'USA_E': {'name': 'USA East', 'remote': 'eastcoast.vpn.blackvpn.com', 'background': 'data/backgrounds/USA_E.png', 'p2p': False, 'stunnel_ports': [23460, 23461]},
    'USA_W': {'name': 'USA West', 'remote': 'westcoast.vpn.blackvpn.com', 'background': 'data/backgrounds/USA_W.png', 'p2p': False, 'stunnel_ports': [23459]}
    }
