#import win api, requires pywin32
import threading
import win32api, win32con, win32gui
import os, sys, traceback
from os.path import dirname, join, abspath

class SystemTray:
    
    destroyed = False
    
    _icon_is_red = False
    _icon_is_amber = False
    _icon_is_green = False
    
    _all_vpns_location_ids = {}
    _all_vpns_names = {}
    _favorites_list = []
    _last_vpn_location_id = ''
    
    def __init__(self):
        self.app = None
        self.hicon = None
        
        msg_TaskbarRestart = win32gui.RegisterWindowMessage("TaskbarCreated");
        message_map = {
                msg_TaskbarRestart: self.OnRestart,
                win32con.WM_DESTROY: self.OnDestroy,
                win32con.WM_COMMAND: self.OnCommand,
                win32con.WM_USER+20 : self.OnTaskbarNotify,
        }
        # Register the Window class.
        wc = win32gui.WNDCLASS()
        hinst = wc.hInstance = win32api.GetModuleHandle(None)
        wc.lpszClassName = "blackTaskbar"
        wc.style = win32con.CS_VREDRAW | win32con.CS_HREDRAW;
        wc.hCursor = win32api.LoadCursor( 0, win32con.IDC_ARROW )
        wc.hbrBackground = win32con.COLOR_WINDOW
        wc.lpfnWndProc = message_map # could also specify a wndproc.

        # Don't blow up if class already registered to make testing easier
        try:
            classAtom = win32gui.RegisterClass(wc)
        except win32gui.error, err_info:
            if err_info.winerror!=winerror.ERROR_CLASS_ALREADY_EXISTS:
                raise

        # Create the Window.
        style = win32con.WS_OVERLAPPED | win32con.WS_SYSMENU
        self.hwnd = win32gui.CreateWindow( wc.lpszClassName, "blackTaskbar", style, \
                0, 0, win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT, \
                0, 0, hinst, None)
        win32gui.UpdateWindow(self.hwnd)
        self._DoCreateIcons()
        
    def _DoCreateIcons(self):
        # Try and find a custom icon
        hinst =  win32api.GetModuleHandle(None)
        #iconPathName = os.path.abspath(os.path.join( os.path.split(sys.executable)[0], "data\\blackvpn.ico" ))
        iconPathName = join(dirname(abspath(__file__)),'data\\blackvpn_32x32.ico')
        if not os.path.isfile(iconPathName):
            # Look in DLLs dir, a-la py 2.5
            iconPathName = os.path.abspath(os.path.join( os.path.split(sys.executable)[0], "DLLs", "pyc.ico" ))
        if not os.path.isfile(iconPathName):
            # Look in the source tree.
            iconPathName = os.path.abspath(os.path.join( os.path.split(sys.executable)[0], "..\\PC\\pyc.ico" ))
        if os.path.isfile(iconPathName):
            icon_flags = win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE
            self.hicon = win32gui.LoadImage(hinst, iconPathName, win32con.IMAGE_ICON, 0, 0, icon_flags)
        else:
            print "Can't find a Python icon file - using default"
            self.hicon = win32gui.LoadIcon(0, win32con.IDI_APPLICATION)

        flags = win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_TIP
        nid = (self.hwnd, 0, flags, win32con.WM_USER+20, self.hicon, "blackVPN")
        try:
            win32gui.Shell_NotifyIcon(win32gui.NIM_ADD, nid)
        except win32gui.error:
            # This is common when windows is starting, and this code is hit
            # before the taskbar has been created.
            print "Failed to add the taskbar icon - is explorer running?"
            # but keep running anyway - when explorer starts, we get the
            # TaskbarCreated message.

    def _DoChangeIcon(self, iconPathName):
        
        if os.path.isfile(iconPathName):
            hinst =  win32api.GetModuleHandle(None)
            icon_flags = win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE
            self.hicon = win32gui.LoadImage(hinst, iconPathName, win32con.IMAGE_ICON, 0, 0, icon_flags)
        else:
            self.app.log("Cannot find icon file: " + iconPathName)
            
        flags = win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_TIP
        nid = (self.hwnd, 0, flags, win32con.WM_USER+20, self.hicon, "blackVPN")
        try:
            win32gui.Shell_NotifyIcon(win32gui.NIM_MODIFY, nid)
        except win32gui.error:
            # This is common when windows is starting, and this code is hit
            # before the taskbar has been created.
            print "Failed to add the taskbar icon - is explorer running?"
            # but keep running anyway - when explorer starts, we get the
            # TaskbarCreated message.

    def _DoShowPopup(self, msg, title):
        try:
            win32gui.Shell_NotifyIcon(win32gui.NIM_MODIFY, \
                             (self.hwnd, 0, win32gui.NIF_INFO, win32con.WM_USER+20,\
                              self.hicon, "Balloon  tooltip", title, 200, msg))
        except win32gui.error:
            # This is common when windows is starting, and this code is hit
            # before the taskbar has been created.
            print "Failed to add the taskbar notification - is explorer running?"
            
            
    def set_running_app(self, app):
        self.app = app
        
    def add_favorite(self, location_id):
        self._favorites_list.append(str(location_id))
        
    def show_popup(self, title, msg):
        thread = threading.Thread(target=self._DoShowPopup, args=(msg, title))
        thread.daemon = True
        thread.start()    
    
    def remove_favorite(self, location_id):
        while location_id in self._favorites_list:
            self._favorites_list.remove(location_id)

    def set_all_vpns_list(self, all_vpns_list):
        systray_id = 1000
        for vpn_location_id in all_vpns_list:
            self._all_vpns_location_ids[vpn_location_id] = str(systray_id)
            self._all_vpns_names[str(systray_id)] = all_vpns_list[vpn_location_id]['name']
            systray_id += 1
            
    def set_last_vpn_location_id(self, location_id):
        self._last_vpn_location_id = location_id

    def setIconGreen(self):
        iconPathName = join(dirname(abspath(__file__)),'data\\bvpn_icon_green.ico')
        self._DoChangeIcon(iconPathName)
        self._icon_is_green = True
        self._icon_is_amber = False
        self._icon_is_red = False
        
    def setIconAmber(self):
        iconPathName = join(dirname(abspath(__file__)),'data\\bvpn_icon_amber.ico')
        self._DoChangeIcon(iconPathName)
        self._icon_is_green = False
        self._icon_is_amber = True
        self._icon_is_red = False
        
    def setIconRed(self):
        iconPathName = join(dirname(abspath(__file__)),'data\\bvpn_icon_red.ico')
        self._DoChangeIcon(iconPathName)
        self._icon_is_green = False
        self._icon_is_amber = False
        self._icon_is_red = True

    def OnRestart(self, hwnd, msg, wparam, lparam):
        self._DoCreateIcons()

    def OnDestroy(self, hwnd, msg, wparam, lparam):
        if self.destroyed is not None and self.destroyed is not True:
            try:
                nid = (self.hwnd, 0)
                win32gui.Shell_NotifyIcon(win32gui.NIM_DELETE, nid)
                win32gui.PostQuitMessage(0) # Terminate the app.
                self.destroyed = True
            except:
                e = sys.exc_info()[0]
                print "SytemTrayOnDestroy() error: " + str(e)
                print traceback.format_exc()

    def kill(self):
        if self.destroyed is not None and self.destroyed is not True:
            try:
                win32gui.DestroyWindow(self.hwnd)
            except:
                e = sys.exc_info()[0]
                print "SytemTray.kill() error: " + str(e)
                print traceback.format_exc()
        
        
    def OnTaskbarNotify(self, hwnd, msg, wparam, lparam):
        if lparam==win32con.WM_LBUTTONUP :
            if self.app.restore_app_window():
                return 1
        #elif lparam==win32con.WM_LBUTTONDBLCLK:
        #    print "You double-clicked me - goodbye"
        #    win32gui.DestroyWindow(self.hwnd)
        if lparam==win32con.WM_RBUTTONUP or lparam==win32con.WM_LBUTTONUP:
            menu = win32gui.CreatePopupMenu()
            
            for vpn_location_id in self._favorites_list:
                vpn_systray_id = self._all_vpns_location_ids[vpn_location_id]
                win32gui.AppendMenu( menu, win32con.MF_STRING, int(vpn_systray_id), "Connect " + self._all_vpns_names[vpn_systray_id])
            
            last_vpn_systray_id = self._all_vpns_location_ids[self._last_vpn_location_id]
            last_vpn_name = self._all_vpns_names[last_vpn_systray_id]
            if self._icon_is_green == False and self._icon_is_amber == False:
                win32gui.AppendMenu( menu, win32con.MF_STRING, 1040, "Connect " + last_vpn_name)
            else:
                win32gui.AppendMenu( menu, win32con.MF_STRING, 1041, "Disconnect " + last_vpn_name)
            
            #win32gui.AppendMenu( menu, win32con.MF_STRING, 1049, "Show App" )
            win32gui.AppendMenu( menu, win32con.MF_STRING, 1050, "Quit App" )
            pos = win32gui.GetCursorPos()
            # See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/menus_0hdi.asp
            win32gui.SetForegroundWindow(self.hwnd)
            win32gui.TrackPopupMenu(menu, win32con.TPM_LEFTALIGN, pos[0], pos[1], 0, self.hwnd, None)
            win32gui.PostMessage(self.hwnd, win32con.WM_NULL, 0, 0)
        return 1

    def OnCommand(self, hwnd, msg, wparam, lparam):
        id = win32api.LOWORD(wparam)
        if id >= 1000 and id <= 1039:
            for location_id in self._all_vpns_location_ids:
                systray_id = self._all_vpns_location_ids[location_id]
                if id == int(systray_id):
                    self.app.do_vpn_connect(location_id)
        elif id == 1040:
            self.app.do_connect_vpn()
        elif id == 1041:
            self.app.do_disconnect_vpn()
        elif id == 1049:
            #BlackvpnApp.show()
            self.app.show_app()
        elif id == 1050:
            print "Goodbye"
            #let the app call our kill() method
            #win32gui.DestroyWindow(self.hwnd)
            self.app.kill()
        else:
            print "Unknown command -", id
