#
# NSIS Installer for BlackVPN
#

# * register OpenVPN configs for blackVPN
# * grant privileges to start/stop OpenVPN service for current user

;--------------------------------
;Globals

!define VERSION_STRING "0.981"
!define VERSION_REG_STRING "0,9,8,1"

!define APPNAME "BlackVPN"
!define PACKAGE_NAME "BlackVPN"
!define PRODUCT_PUBLISHER "BlackVPN, Inc."

!define PROJECT_DIR "..\.."
!define INSTALLER_DIR "${PROJECT_DIR}\blackvpn-build\nsis-installer"
!define OPENVPN_DIR "${INSTALLER_DIR}\openvpn-2.4.4"
!define TAP_WINDOWS_INSTALLER "${INSTALLER_DIR}\tap-windows.exe"

!define STUNNEL_INSTALLER "${INSTALLER_DIR}\stunnel\stunnel-5.43-win32-installer.exe"
!define STUNNEL_CONF "${INSTALLER_DIR}\stunnel\stunnel.conf"
!define STUNNEL_CERT "${INSTALLER_DIR}\stunnel\client.pem"

!define SETUP_EXE "..\output\blackvpn-setup.exe"

;--------------------------------

; import SimpleSC
!addplugindir .

SetCompressor /SOLID lzma

; Modern user interface
!include "MUI2.nsh"

; Install for all users. MultiUser.nsh also calls SetShellVarContext to point 
; the installer to global directories (e.g. Start menu, desktop, etc.)
!define MULTIUSER_EXECUTIONLEVEL Admin
!include "MultiUser.nsh"

; WinMessages.nsh is needed to send WM_CLOSE to the GUI if it is still running
!include "WinMessages.nsh"

; x64.nsh for architecture detection
!include "x64.nsh"

; nsProcess.nsh to detect whether OpenVPN process is running ( http://nsis.sourceforge.net/NsProcess_plugin )
!include "nsProcess.nsh"

; DotNetChecker.nsh to detect whether .net 4.0 is enabled, which is required for openvpnserv2 ( https://github.com/ReVolly/NsisDotNetChecker )
!include "DotNetChecker.nsh"

; Read the command-line parameters
!insertmacro GetParameters
!insertmacro GetOptions

; Default service settings
!define OPENVPN_CONFIG_EXT "ovpn"

;--------------------------------
;Configuration


; Package name as shown in the installer GUI
Name "${PACKAGE_NAME} ${VERSION_STRING}"

; On 64-bit Windows the constant $PROGRAMFILES defaults to
; C:\Program Files (x86) and on 32-bit Windows to C:\Program Files. However,
; the .onInit function (see below) takes care of changing this for 64-bit 
; Windows.
InstallDir "$PROGRAMFILES\${PACKAGE_NAME}"

; Installer filename
OutFile ${SETUP_EXE}

ShowInstDetails show
ShowUninstDetails show

;Remember install folder
InstallDirRegKey HKLM "SOFTWARE\${PACKAGE_NAME}" ""

;--------------------------------
;Modern UI Configuration

; Compile-time constants which we'll need during install
!define MUI_WELCOMEPAGE_TEXT "This wizard will guide you through the installation of ${PACKAGE_NAME} ${VERSION_STRING}.$\r$\n$\r$\nNote that the version will only run on Windows Vista, or higher.$\r$\n$\r$\n$\r$\n"

!define MUI_COMPONENTSPAGE_TEXT_TOP "Select the components to install/upgrade.  Stop ${PACKAGE_NAME} process if it is running.  All DLLs are installed locally."
!define MUI_COMPONENTSPAGE_SMALLDESC

!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_FINISHPAGE_RUN "$INSTDIR\blackvpn\blackvpn.exe"

!define MUI_ABORTWARNING
!define MUI_ICON "${PROJECT_DIR}\app.ico"
!define MUI_UNICON "${PROJECT_DIR}\app.ico"
;!define MUI_HEADERIMAGE
;!define MUI_HEADERIMAGE_BITMAP "install-whirl.bmp"
!define MUI_UNFINISHPAGE_NOAUTOCLOSE

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "terms.txt"
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages
 
!insertmacro MUI_LANGUAGE "English"
  
;--------------------------------
;Language Strings

LangString DESC_SecBlackVPN ${LANG_ENGLISH} "Install ${PACKAGE_NAME}"
LangString DESC_SecAddShortcuts ${LANG_ENGLISH} "Add ${PACKAGE_NAME} shortcuts to the current user's Start Menu."
LangString DESC_SecLaunchGUIOnLogon ${LANG_ENGLISH} "Launch ${PACKAGE_NAME} GUI on user logon."
  
;--------------------------------
;Macros

!macro SelectByParameter SECT PARAMETER DEFAULT
	${GetOptions} $R0 "/${PARAMETER}=" $0
	${If} ${DEFAULT} == 0
		${If} $0 == 1
			!insertmacro SelectSection ${SECT}
		${EndIf}
	${Else}
		${If} $0 != 0
			!insertmacro SelectSection ${SECT}
		${EndIf}
	${EndIf}
!macroend

!macro WriteRegStringIfUndef ROOT SUBKEY KEY VALUE
	Push $R0
	ReadRegStr $R0 "${ROOT}" "${SUBKEY}" "${KEY}"
	${If} $R0 == ""
		WriteRegStr "${ROOT}" "${SUBKEY}" "${KEY}" '${VALUE}'
	${EndIf}
	Pop $R0
!macroend

!macro WriteRegDWORDIfUndef ROOT SUBKEY KEY VALUE
	Push $R0
	ReadRegDWORD $R0 "${ROOT}" "${SUBKEY}" "${KEY}"
	${If} $R0 == ""
		WriteRegDWORD "${ROOT}" "${SUBKEY}" "${KEY}" '${VALUE}'
	${EndIf}
	Pop $R0
!macroend

!macro DelRegKeyIfUnchanged ROOT SUBKEY VALUE
	Push $R0
	ReadRegStr $R0 "${ROOT}" "${SUBKEY}" ""
	${If} $R0 == '${VALUE}'
		DeleteRegKey "${ROOT}" "${SUBKEY}"
	${EndIf}
	Pop $R0
!macroend

!macro StopBlackVPN un
	Function ${un}StopBlackVPN
		${nsProcess::FindProcess} "blackvpn.exe" $R0

		${If} $R0 == 0
			DetailPrint "${APPNAME} is running. Closing it down"
			${nsProcess::KillProcess} "blackvpn.exe" $R0
			Sleep 1000
		${EndIf}
		${nsProcess::Unload}	
	FunctionEnd
!macroend

!insertmacro StopBlackVPN ""
!insertmacro StopBlackVPN "un."

Function SetStartAndAutostartForService
	SimpleSC::ExistsService "OpenVPNService"
	Pop $0

	${If} $0 == 0
		DetailPrint "Set autostart to OpenVPN Service"
		SimpleSC::SetServiceStartType "OpenVPNService" 2

		SimpleSC::GetServiceStatus "OpenVPNService"
		Pop $0
		Pop $1
		${If} $1 != 4
			DetailPrint "Start OpenVPN Service"
			SimpleSC::StartService "OpenVPNService" "" 10
		${EndIf}
	${EndIf}
FunctionEnd

Function StopService
	SimpleSC::ExistsService "OpenVPNService"
	Pop $0

	${If} $0 == 0
		DetailPrint "Stopping OpenVPN service"
		SimpleSC::StopService "OpenVPNService" 0 10
	${EndIf}
FunctionEnd

Function InstallOpenVPNService

	; TAP-windows: OpenVPN service will not work without it
	
	DetailPrint "Installing TAP (may need confirmation)..."
	SetOutPath "$TEMP"
	File /oname=tap-windows.exe "${TAP_WINDOWS_INSTALLER}"

	nsExec::ExecToLog /OEM '"$TEMP\tap-windows.exe" /S /SELECT_UTILITIES=1'
	Pop $R0 # return value/error/timeout

	Delete "$TEMP\tap-windows.exe"

	; OpenVPN service
	
	DetailPrint "Installing OpenVPN Service..."
	
	; set registry parameters for service (copied from OpenVPN installer)
	;!insertmacro WriteRegStringIfUndef HKLM "SOFTWARE\OpenVPN" "config_dir" "$INSTDIR\config" 
	!insertmacro WriteRegStringIfUndef HKLM "SOFTWARE\OpenVPN" "config_ext"  "${OPENVPN_CONFIG_EXT}"
	WriteRegStr                        HKLM "SOFTWARE\OpenVPN" "exe_path"    "$INSTDIR\openvpn\bin\openvpn.exe"
	!insertmacro WriteRegStringIfUndef HKLM "SOFTWARE\OpenVPN" "log_dir"     "$INSTDIR\openvpn\log"
	!insertmacro WriteRegStringIfUndef HKLM "SOFTWARE\OpenVPN" "priority"    "NORMAL_PRIORITY_CLASS"
	!insertmacro WriteRegStringIfUndef HKLM "SOFTWARE\OpenVPN" "log_append"  "0"
	!insertmacro WriteRegStringIfUndef HKLM "SOFTWARE\OpenVPN" "ovpn_admin_group" "OpenVPN Administrators"
	!insertmacro WriteRegDWORDIfUndef  HKLM "SOFTWARE\OpenVPN" "disable_save_passwords"  0
	WriteRegStr                        HKLM "SOFTWARE\OpenVPN" "" "$INSTDIR\openvpn"

	SetOutPath "$INSTDIR\openvpn"
	File /r "${OPENVPN_DIR}\*.*"

	SimpleSC::InstallService "OpenVPNService" "OpenVPNService" "16" "2" '"$INSTDIR\openvpn\bin\openvpnserv2.exe"' "tap0901/dhcp" "" ""
	Pop $0 ; returns an errorcode (<>0) otherwise success (0)
	
FunctionEnd

Function InstallStunnelIfRequired
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "stunnel" "not_installed"
	
	ReadRegStr $R9 HKLM32 "SOFTWARE\NSIS_stunnel" "Install_Dir"
	${If} $R9 == ""
		DetailPrint "Installing Stunnel..."
		SetOutPath "$TEMP"
		File /oname=stunnel-installer.exe "${STUNNEL_INSTALLER}"

		nsExec::ExecToLog /OEM '"$TEMP\stunnel-installer.exe" /S'
		Pop $R0 # return value/error/timeout

		Delete "$TEMP\stunnel-installer.exe"
		ReadRegStr $R9 HKLM32 "SOFTWARE\NSIS_stunnel" "Install_Dir"
	${Endif}
	
	DetailPrint "Stunnel installation dir: $R9"
	
	; install config files
	SetOverwrite on
	SetOutPath "$R9\config"
	File ${STUNNEL_CONF}
	File ${STUNNEL_CERT}
	
	; start stunnel service
	SimpleSC::StartService "stunnel" "" 10
	
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "stunnel" "installed"
	
FunctionEnd

Function un.UninstallStunnelIfRequired
	ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "stunnel"
	${If} $R0 == "installed"
		ReadRegStr $R0 HKLM32 "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\stunnel" "UninstallString"
		${If} $R0 != ""
			DetailPrint "Uninstalling stunnel..."
			nsExec::ExecToLog /OEM '$R0 /S'
			Pop $R0 # return value/error/timeout
		${EndIf}
	${EndIf}
	
FunctionEnd

Function un.DisableKillSwitchIfRequired
	nsExec::ExecToLog /OEM 'netsh advfirewall firewall show rule name="BlackVPN-EnableOpenVPN"'
    Pop $R0 # return value/error/timeout
	${If} $R0 == "0"
        DetailPrint "Disabling killswitch..."
        nsExec::ExecToLog /OEM 'netsh advfirewall import "$INSTDIR\blackvpn\firewall_default_settings.wfw"'
        Pop $R0 # return value/error/timeout
        DetailPrint "Result of restoring firewall rules: $R0"
	${EndIf}
	
FunctionEnd

; StrContains
; This function does a case sensitive searches for an occurrence of a substring in a string. 
; It returns the substring if it is found. 
; Otherwise it returns null(""). 
; Written by kenglish_hi
; Adapted from StrReplace written by dandaman32
 
 
Var STR_HAYSTACK
Var STR_NEEDLE
Var STR_CONTAINS_VAR_1
Var STR_CONTAINS_VAR_2
Var STR_CONTAINS_VAR_3
Var STR_CONTAINS_VAR_4
Var STR_RETURN_VAR
 
Function StrContains
  Exch $STR_NEEDLE
  Exch 1
  Exch $STR_HAYSTACK
  ; Uncomment to debug
  ;MessageBox MB_OK 'STR_NEEDLE = $STR_NEEDLE STR_HAYSTACK = $STR_HAYSTACK '
    StrCpy $STR_RETURN_VAR ""
    StrCpy $STR_CONTAINS_VAR_1 -1
    StrLen $STR_CONTAINS_VAR_2 $STR_NEEDLE
    StrLen $STR_CONTAINS_VAR_4 $STR_HAYSTACK
    loop:
      IntOp $STR_CONTAINS_VAR_1 $STR_CONTAINS_VAR_1 + 1
      StrCpy $STR_CONTAINS_VAR_3 $STR_HAYSTACK $STR_CONTAINS_VAR_2 $STR_CONTAINS_VAR_1
      StrCmp $STR_CONTAINS_VAR_3 $STR_NEEDLE found
      StrCmp $STR_CONTAINS_VAR_1 $STR_CONTAINS_VAR_4 done
      Goto loop
    found:
      StrCpy $STR_RETURN_VAR $STR_NEEDLE
      Goto done
    done:
   Pop $STR_NEEDLE ;Prevent "invalid opcode" errors and keep the
   Exch $STR_RETURN_VAR  
FunctionEnd
 
!macro _StrContainsConstructor OUT NEEDLE HAYSTACK
  Push `${HAYSTACK}`
  Push `${NEEDLE}`
  Call StrContains
  Pop `${OUT}`
!macroend
 
!define StrContains '!insertmacro "_StrContainsConstructor"'

;--------------------
;Pre-install section


Section -pre
	Call StopBlackVPN
	Call StopService

	Sleep 3000

	; GUI not running/closed successfully, carry on with install/upgrade

	; Delete previous start menu folder
	RMDir /r "$SMPROGRAMS\${PACKAGE_NAME}"

	Pop $0 ; for FindWindow

SectionEnd

Section /o "Add Shortcuts to Start Menu" SecAddShortcuts
	SetOverwrite on
	CreateDirectory "$SMPROGRAMS\${PACKAGE_NAME}\Documentation"
	WriteINIStr "$SMPROGRAMS\${PACKAGE_NAME}\Documentation\${PACKAGE_NAME} FAQ.url" "InternetShortcut" "URL" "https://support.blackvpn.com/kb/index.php"
	WriteINIStr "$SMPROGRAMS\${PACKAGE_NAME}\Documentation\${PACKAGE_NAME} Website.url" "InternetShortcut" "URL" "https://www.blackvpn.com/"
	WriteINIStr "$SMPROGRAMS\${PACKAGE_NAME}\Documentation\${PACKAGE_NAME} Support.url" "InternetShortcut" "URL" "https://support.blackvpn.com/index.php"

	CreateShortCut "$SMPROGRAMS\${PACKAGE_NAME}\Uninstall ${PACKAGE_NAME}.lnk" "$INSTDIR\Uninstall.exe"
SectionEnd

Section /o "Launch ${PACKAGE_NAME} GUI on User Logon" SecLaunchGUIOnLogon
SectionEnd

; We do not make this hidden as its informative to have displayed, but make it readonly (always selected)
Section /o "${PACKAGE_NAME}" SecBlackVPN

	SectionIn RO ; section cannot be unchecked by user
	SetOverwrite on

	SetOutPath $INSTDIR
	File "${PROJECT_DIR}\app.ico"

	DetailPrint "Unpacking BlackVPN GUI binaries..."
	
	SetOutPath "$INSTDIR\blackvpn"
	File /r "..\output\dist\blackvpn\*.*"
	File "StartBlackVPN.vbs"
	
	SetOutPath "$INSTDIR\configs"
	File /r "${PROJECT_DIR}\configs\*.*"

	; check OpenVPN service is installed already or install it	
	DetailPrint "Checking OpenVPNService"
	
	SimpleSC::ExistsService "OpenVPNService"
	Pop $0 ; an errorcode if the service doesn't exists (<>0)
	IntCmp $0 0 Exist
		Call InstallOpenVPNService
		WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "openvpn" "installed"
	Exist:
	
	Call InstallStunnelIfRequired
	
	${If} ${SectionIsSelected} ${SecAddShortcuts}
		CreateDirectory "$SMPROGRAMS\${PACKAGE_NAME}"
		CreateShortCut "$SMPROGRAMS\${PACKAGE_NAME}\${PACKAGE_NAME}.lnk" "$INSTDIR\blackvpn\blackvpn.exe" ""
		CreateShortCut "$DESKTOP\${PACKAGE_NAME}.lnk" "$INSTDIR\blackvpn\blackvpn.exe"
	${EndIf}

	ReadRegStr $0 HKLM "SOFTWARE\OpenVPN" "config_dir"
	${StrContains} $1 "${PACKAGE_NAME}" "$0"
	StrCmp $1 "" 0 configDirReplaced
		DetailPrint "Patch OpenVPN config dir, to be restored on uninstall"
		WriteRegStr HKLM "SOFTWARE\${PACKAGE_NAME}" "config_dir_backup" $0
	configDirReplaced:
	
	; register new configs dir for OpenVPN service
	WriteRegStr HKLM "SOFTWARE\OpenVPN" "config_dir" "$INSTDIR\configs" 
	
	; set registry parameters
	!insertmacro WriteRegStringIfUndef HKLM "SOFTWARE\${PACKAGE_NAME}" "exe_path"    "$INSTDIR\blackvpn\blackvpn.exe"
	!insertmacro WriteRegStringIfUndef HKLM "SOFTWARE\${PACKAGE_NAME}" "priority"    "NORMAL_PRIORITY_CLASS"

	; Using active setup registry entries to set/unset GUI to launch on logon for each user.
	; If the user removes the GUI from startup items it will not be re-added or removed on subsequent
	; installs unless the value of "Version" is updated (do this only if/when really necessary).
	; Ref: https://helgeklein.com/blog/2010/04/active-setup-explained/
	WriteRegStr HKLM "Software\Microsoft\Active Setup\Installed Components\${PACKAGE_NAME}_UserSetup" "" "BlackVPN Setup"
	WriteRegStr HKLM "Software\Microsoft\Active Setup\Installed Components\${PACKAGE_NAME}_UserSetup" "Version" ${VERSION_REG_STRING}
	WriteRegDword HKLM "Software\Microsoft\Active Setup\Installed Components\${PACKAGE_NAME}_UserSetup" "IsInstalled" 0x1
        ; DontAsk = 2 is used to not prompt the user
	WriteRegDword HKLM "Software\Microsoft\Active Setup\Installed Components\${PACKAGE_NAME}_UserSetup" "DontAsk" 0x2
	${If} ${SectionIsSelected} ${SecLaunchGUIOnLogon}
		WriteRegStr HKLM "Software\Microsoft\Active Setup\Installed Components\${PACKAGE_NAME}_UserSetup" "StubPath" "reg add HKCU\Software\Microsoft\Windows\CurrentVersion\Run /v BLACKVPN /t REG_SZ /d $\"$INSTDIR\blackvpn\StartBlackVPN.vbs$\" /f"
	${Else}
		WriteRegStr HKLM "Software\Microsoft\Active Setup\Installed Components\${PACKAGE_NAME}_UserSetup" "StubPath" "reg delete HKCU\Software\Microsoft\Windows\CurrentVersion\Run /v BLACKVPN /f"
	${EndIf}
SectionEnd

;--------------------------------
;Installer Sections

Function .onInit
	${GetParameters} $R0
	ClearErrors

	${IfNot} ${AtLeastWinVista}
		MessageBox MB_YESNO|MB_ICONEXCLAMATION "This package does not work on your operating system. You should have Windows Vista at least."
		Quit
	${EndIf}

	${If} ${RunningX64}
		SetRegView 64
		; Change the installation directory to C:\Program Files, but only if the
		; user has not provided a custom install location.
		${If} "$INSTDIR" == "$PROGRAMFILES\${PACKAGE_NAME}"
			StrCpy $INSTDIR "$PROGRAMFILES64\${PACKAGE_NAME}"
		${EndIf}
	${EndIf}

	!insertmacro SelectByParameter ${SecBlackVPN} SELECT_BLACKVPN 1
	!insertmacro SelectByParameter ${SecAddShortcuts} SELECT_SHORTCUTS 1
	!insertmacro SelectByParameter ${SecLaunchGUIOnLogon} SELECT_LAUNCH 1

	!insertmacro MULTIUSER_INIT
	SetShellVarContext all

FunctionEnd

;--------------------
;Post-install section

Section -post

	SetOverwrite on

	; Store install folder in registry
	WriteRegStr HKLM "SOFTWARE\${PACKAGE_NAME}" "" "$INSTDIR"

	; Create uninstaller
	WriteUninstaller "$INSTDIR\Uninstall.exe"

	; Show up in Add/Remove programs
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "DisplayName" "${PACKAGE_NAME} ${VERSION_STRING}"
	WriteRegExpandStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "UninstallString" "$INSTDIR\Uninstall.exe"
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "DisplayIcon" "$INSTDIR\app.ico"
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "DisplayVersion" "${VERSION_STRING}"
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "HelpLink" "https://www.blackvpn.com/support/"
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "InstallLocation" "$INSTDIR\"
	WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "Language" 1033
	WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "NoModify" 1
	WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "NoRepair" 1
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "Publisher" "${PRODUCT_PUBLISHER}"
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "UninstallString" "$INSTDIR\Uninstall.exe"
	WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "URLInfoAbout" "https://www.blackvpn.com/about/"

	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
	IntFmt $0 "0x%08X" $0
	WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "EstimatedSize" "$0"

	Call SetStartAndAutostartForService

	ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "openvpn"
	${If} $R0 == "installed"
		!insertmacro CheckNetFramework 40Full
	${Endif}

SectionEnd

;--------------------------------
;Uninstaller Section

Function un.onInit
	ClearErrors
	!insertmacro MULTIUSER_UNINIT
	SetShellVarContext all
	
	${If} ${RunningX64}
		SetRegView 64
	${EndIf}

FunctionEnd

Section "Uninstall"
	; Stop BlackVPN if currently running
	Call un.StopBlackVPN

	ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}" "openvpn"
	${If} $R0 == "installed"
		; Get the binary path of a service
		SimpleSC::GetServiceBinaryPath "OpenVPNService"
		Pop $0 ; returns an errorcode (<>0) otherwise success (0)
		Pop $1 ; returns the binary path of the service
		StrCmp $1 '"$INSTDIR\openvpn\bin\openvpnserv2.exe"' 0 NoNeedToUninstallService
			; Service have to be explicitly stopped before they are removed
			DetailPrint "Stopping OpenVPN Service..."
			SimpleSC::StopService "OpenVPNService" 0 10
			DetailPrint "Removing OpenVPN Service..."
			SimpleSC::RemoveService "OpenVPNService"
			
			DeleteRegKey HKLM "SOFTWARE\OpenVPN"
			Sleep 3000
		NoNeedToUninstallService:
	
		ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\TAP-Windows" "UninstallString"
		${If} $R0 != ""
			DetailPrint "Uninstalling TAP..."
			nsExec::ExecToLog /OEM '"$R0" /S'
			Pop $R0 # return value/error/timeout
		${EndIf}
	${EndIf}

	Call un.UninstallStunnelIfRequired
    Call un.DisableKillSwitchIfRequired

	; restore old OpenVPN config dir
	ReadRegStr $0 HKLM "SOFTWARE\${PACKAGE_NAME}" "config_dir_backup"
	${If} $0 != ""
		ReadRegStr $1 HKLM "SOFTWARE\OpenVPN" "config_dir"
		${If} $1 != ""
			WriteRegStr HKLM "SOFTWARE\OpenVPN" "config_dir" $0 
		${EndIf}
	${EndIf}
	
	Delete "$DESKTOP\${PACKAGE_NAME}.lnk"
	RMDir /r "$SMPROGRAMS\${PACKAGE_NAME}"

	RMDir /r "$INSTDIR\blackvpn"
	; workaround for the problem dir are not removed sometimes if there are many files in it
	RMDir "$INSTDIR\blackvpn"
	RMDir /r "$INSTDIR\openvpn"
	; workaround for the problem dir are not removed sometimes if there are many files in it
	RMDir "$INSTDIR\openvpn"
	RMDir /r "$INSTDIR\configs"
	RMDir /r "$INSTDIR\log"

	Delete "$INSTDIR\app.ico"
	Delete "$INSTDIR\Uninstall.exe"
	
	RMDir "$INSTDIR"

	DeleteRegKey HKLM "SOFTWARE\${PACKAGE_NAME}"
	DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PACKAGE_NAME}"
	; Set installed status to 0 in Active Setup
	WriteRegDword HKLM "Software\Microsoft\Active Setup\Installed Components\${PACKAGE_NAME}_UserSetup" "IsInstalled" 0x0
	WriteRegStr HKLM "Software\Microsoft\Active Setup\Installed Components\${PACKAGE_NAME}_UserSetup" "StubPath" "reg delete HKCU\Software\Microsoft\Windows\CurrentVersion\Run /v BLACKVPN /f"
	
SectionEnd


