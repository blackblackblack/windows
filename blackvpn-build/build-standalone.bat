rmdir /s /q "output/build"
rmdir /s /q "output/dist"

set BLACKVPNPATH=%~dp0..\
python -m PyInstaller standalone/blackvpn.spec --distpath output/dist --workpath output/build && xcopy /Y "standalone/precompiled" "output/dist/blackvpn" && "../keys/sign.bat" "output/dist/blackvpn/blackvpn.exe" && echo "=====> output/dist/blackvpn"

