# -*- mode: python -*-

import os
from kivy.tools.packaging.pyinstaller_hooks import get_deps_minimal, hookspath

def get_deps():
	# workaround for the problem kivy window shown on getting deps: hide kivy window via config
	from kivy.config import Config
	Config.set('graphics', 'window_state', 'hidden')

	kivy_deps = get_deps_minimal(video=None, audio=None, spelling=None, camera=None, window=True)
	excludes = kivy_deps['excludes']
	excludes.remove('twisted')
	excludes.remove('Tkinter')
	excludes.remove('_tkinter')
	excludes.append('win32com')
	hiddenimports = kivy_deps['hiddenimports']

	return (excludes, hiddenimports)

block_cipher = None

app_path = os.environ['BLACKVPNPATH']
print app_path
data_folder = Tree(os.path.join(app_path, 'data'), prefix='data/')

(excludes, hiddenimports) = get_deps()

# workaround for the problem kivy window shown on hooks loading.
# the only way I found is to disable default kivy hooks, that are not really used
# then enable it back after the build finished
try:
	kivy_hooks_path = os.path.join(HOMEPATH, "PyInstaller/hooks/hook-kivy.py")
	if os.path.exists(kivy_hooks_path):
		os.rename(kivy_hooks_path, kivy_hooks_path + ".bak")
except:
	pass

icon_path = os.path.join(app_path, 'app.ico')
try:
	a = Analysis([os.path.join(app_path, 'start.py')],
				 pathex=[os.path.join(app_path, 'blackvpn-build/output')],
				 binaries=[],
				 datas=[(os.path.join(app_path, 'windows.kv'),'.'), (icon_path,'.')],
				 hiddenimports=hiddenimports,
				 hookspath=hookspath(),
				 runtime_hooks=[],
				 excludes=excludes,
				 win_no_prefer_redirects=False,
				 win_private_assemblies=False,
				 cipher=block_cipher)
	pyz = PYZ(a.pure, a.zipped_data,
				 cipher=block_cipher)
	exe = EXE(pyz,
			  a.scripts,
			  exclude_binaries=True,
			  name='blackvpn',
			  debug=False,
			  strip=False,
			  upx=True,
			  console=False,
			  icon=icon_path)
			  
			
	from kivy.deps import sdl2, glew		
	coll = COLLECT(exe,
				   a.binaries,
				   a.zipfiles,
				   a.datas,
				   data_folder,
				   *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
				   strip=False,
				   upx=True,
				   name='blackvpn')

finally:
	try:
		if os.path.exists(kivy_hooks_path + ".bak"):
			os.rename(kivy_hooks_path + ".bak", kivy_hooks_path)
	except:
		pass