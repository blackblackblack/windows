#coding: utf-8

import kivy
kivy.require('1.10.0')

from kivy.config import Config
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', '300')
Config.set('graphics', 'height', '600')

# add the following 2 lines to solve OpenGL 2.0 bug
from kivy import Config
Config.set('graphics', 'multisamples', '0')
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

from kivy.app import App
from kivy.base import stopTouchApp
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.graphics import *
from kivy.network.urlrequest import UrlRequest
from kivy.properties import StringProperty
from kivy.uix.screenmanager import *
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.bubble import Bubble, BubbleButton
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup

# OS imports
from os.path import join
from functools import partial
import subprocess
import datetime
import os
import keyring
import urllib
import json
import platform
import base64
import threading
import tempfile
import hashlib
import re

# fix for pyinstaller packages app to avoid ReactorAlreadyInstalledError
import traceback
import sys
if 'twisted.internet.reactor' in sys.modules:
    del sys.modules['twisted.internet.reactor']

# Twisted Queue.Empty exception
from multiprocessing import Queue
from Queue import Empty

# import our own modules
from globals import *
from funcs import *
from killswitch import KillSwitch


class MenuBubble(Bubble):
    
    pos_x = 0
    pos_y = 0
    pos_hidden_x = -300
    pos_hidden_y = 0
    
    def __init__(self, **kwargs):
        super(MenuBubble, self).__init__(**kwargs)
    
    def is_hidden(self):
        return True if (self.x == self.pos_hidden_x and self.y == self.pos_hidden_y) else False
    
    def show(self):
        
        if self.pos_x != 0 and self.pos_y != 0:
            self.x = self.pos_x
            self.y = self.pos_y
            
    def hide(self):
        
        if self.x != 0 and self.y != 0:
            self.pos_x = self.x
            self.pos_y = self.y
            self.x = self.pos_hidden_x
            self.y = self.pos_hidden_y

class ImageButton(ButtonBehavior, Image):
    
    image_on = StringProperty()
    image_off = StringProperty()
    _is_on = False
    
    def __init__(self, **kwargs):
        super(ImageButton, self).__init__(**kwargs)
        self.turn_off()

    def is_on(self):
        return self._is_on
        
    def show_on(self):
        self.source = self.image_on
        
    def show_off(self):
        self.source = self.image_off

    def turn_on(self):
        self._is_on = True
        self.show_on()

    def turn_off(self):
        self._is_on = False
        self.show_off()
        
class ThreeStateImageButton(ButtonBehavior, Image):
    
    image_on = StringProperty()
    image_amber = StringProperty()
    image_off = StringProperty()
    
    _state_is_on = False
    _state_is_amber = False
    _state_is_off = False
    
    def __init__(self, **kwargs):
        super(ThreeStateImageButton, self).__init__(**kwargs)
        self.set_off()
        
    def set_on(self):
        if not self.is_on():
            self.source = self.image_on
            self._state_is_on = True
            self._state_is_amber = False
            self._state_is_off = False
        
    def set_amber(self):
        if not self.is_amber():
            self.source = self.image_amber
            self._state_is_on = False
            self._state_is_amber = True
            self._state_is_off = False
        
    def set_off(self):
        if not self.is_off():
            self.source = self.image_off
            self._state_is_on = False
            self._state_is_amber = False
            self._state_is_off = True

    def is_on(self):
        return self._state_is_on
        
    def is_amber(self):
        return self._state_is_amber
        
    def is_off(self):
        return self._state_is_off

        
class NotificationIcon(ButtonBehavior, Image):

    image_off = StringProperty()
    image_red = StringProperty()
    image_amber = StringProperty()
    image_green = StringProperty()
    
    STATE_OFF = 0
    STATE_RED = 1
    STATE_AMBER = 2
    STATE_GREEN = 3
    
    _state = 0

    def __init__(self, **kwargs):
        super(NotificationIcon, self).__init__(**kwargs)
        
    def show_state(self):
        print self._state

    def is_off(self):
        return True if self._state == self.STATE_OFF else False
        
    def is_red(self):
        return True if self._state == self.STATE_RED else False
        
    def is_amber(self):
        return True if self._state == self.STATE_AMBER else False
        
    def is_green(self):
        return True if self._state == self.STATE_GREEN else False

    def turn_off(self):
        if self._state != self.STATE_OFF:
            self._state = self.STATE_OFF
            self.source = self.image_off
        
    def turn_red(self):
        if self._state != self.STATE_RED:
            self._state = self.STATE_RED
            self.source = self.image_red
        
    def turn_amber(self):
        if self._state != self.STATE_AMBER:
            self._state = self.STATE_AMBER
            self.source = self.image_amber
        
    def turn_green(self):
        if self._state != self.STATE_GREEN:
            self._state = self.STATE_GREEN
            self.source = self.image_green
        

class VpnListLabel(Label):
    vpn_id = StringProperty()
    
    _vpn_name = ''
    _is_highlighted = False
        
    def __init__(self, **kwargs):
        super(VpnListLabel, self).__init__(**kwargs)
        # since we are initialising via the .kv file then no properties are set yet
        # schedule initialization for later when the properites have been set
        Clock.schedule_once(self.do_init, 0)
        
    # this gets done after .kv is parsed
    def do_init(self, *largs):
        if self.vpn_id is None or len(self.vpn_id) == 0:
            self.text = ''
        else:
            self.markup = True
            self._vpn_name = App.get_running_app().get_vpn_name(self.vpn_id) + '   '
            if App.get_running_app().is_vpn_favorite(self.vpn_id):
                self.set_highlight_on()
            else:
                self.set_highlight_off()
        
    def on_ref_press(self, ref):
        if self._is_highlighted is True:
            self.set_highlight_off()
            App.get_running_app().vpn_favorite_remove(self.vpn_id)
        else:
            self.set_highlight_on()
            App.get_running_app().vpn_favorite_add(self.vpn_id)
        
    def set_highlight_off(self):
        self.text = '[ref=' + str(self.vpn_id) + ']' + str(self._vpn_name) + '[/ref]'
        self._is_highlighted = False
        
    def set_highlight_on(self):        
        self.text = '[color=ff5555]' + '[ref=' + str(self.vpn_id) + ']' + str(self._vpn_name) + '[/ref]' + '[/color]'
        self._is_highlighted = True
        

class WindowsWidget(BoxLayout):
    
    _connected_time_event = None
    _connected_time_start = None
    
    _speed_down_reset_event = None
    _speed_up_reset_event = None
    
    _setting_show_notifications = False
    
    def __init__(self, **kwargs):
        super(WindowsWidget, self).__init__(**kwargs)
        #self.layout_content.bind(minimum_height=self.layout_content.setter('height'))
    
    def build(self):
        pass
        
    def initialize(self):
            
        self.ids.menu.hide()
        self.ids.menu_favs.hide()
        
        self.ids.menu_button.turn_off()
        self.ids.button_all_vpns.turn_off()
        self.ids.hearts_button.turn_off()
        
        self.ids.icon_killswitch.turn_off()
        self.ids.icon_dnsleak.turn_off()
        self.ids.icon_p2p.turn_off()
        self.ids.icon_alerts.turn_off()
        
        self.ids.bubble_killswitch.hide()
        self.ids.bubble_dnsleak.hide()
        self.ids.bubble_p2p.hide()
        self.ids.bubble_alerts.hide()
                
        self.hide_sidebar()
        self.ids.sm.transition = NoTransition()
        self.ids.sm_logs.transition = NoTransition()
        
        self.hide_vpn_speeds()
        self.hide_connected_time()
        
        self.ids.bubble_username_password.hide()
        
        self.ids.support_send_button.set_on()
        
        self.ids.button_check_app_updates.turn_on()
        self.ids.button_install_app_updates.turn_off()
    
    def init_to_connect(self):
        self.hide_sidebar()
        self.ids.menu_button.turn_off()
        self.ids.menu.hide()
        self.ids.button_all_vpns.turn_off()
        self.ids.hearts_button.turn_off()
        self.ids.menu_favs.hide()
        self.hide_vpn_speeds()
        self.hide_connected_time()
        
    def init_disconnected(self):
        self.ids.label_vpn_status.text = "Not Connected"
        self.hide_vpn_speeds()
        self.hide_connected_time()
        self.ids.image_vpn_speed.show_off()
        self.reset_notification_p2p()
        self.ids.button_vpn_switch.set_off()
        
        self.show_notification_disconnected()
        
    def init_connecting(self):
        self.ids.button_vpn_switch.set_amber()
        if self.ids.label_vpn_status.text != "Connecting":
            self.ids.label_vpn_status.text = "Connecting"
            
    def init_connected(self, vpn_location_id, vpn_protocol, vpn_port, vpn_remote, do_not_show_connected_popup):
        self.ids.button_vpn_switch.set_on()
        self.ids.label_vpn_status.text = "Connected"
        self.ids.image_vpn_speed.show_on()
        self.reset_connected_time()
        
        if not do_not_show_connected_popup:
            self.show_notification_connected()

    def update_notification_p2p(self, p2p_allowed):
        if p2p_allowed:
            self.ids.icon_p2p.turn_green()
            self.ids.p2p_bubble_button.text = 'P2P/bittorrent\ntraffic is\nunrestricted.\n'
        else:
            self.ids.icon_p2p.turn_red()
            self.ids.p2p_bubble_button.text = 'P2P/bittorrent\ntraffic is\nrestricted.\n'
            
    def reset_notification_p2p(self):
        self.ids.icon_p2p.turn_off()
        self.ids.p2p_bubble_button.text = ''
        self.ids.bubble_p2p.hide()
        
    def update_notification_alerts(self, colour, message):
        if colour == 'green':
            self.ids.icon_alerts.turn_green()
            self.ids.alerts_bubble_button.text = str(message)
        else:
            self.ids.icon_alerts.turn_red()
            self.ids.alerts_bubble_button.text = str(message)
            
        self.ids.bubble_alerts.show()
        
    def reset_notification_alerts(self):
        self.ids.icon_alerts.turn_off()
        self.ids.alerts_bubble_button.text = ''
        self.ids.bubble_alerts.hide()
        
    def show_sidebar(self):
        Window.size = (750, 600)

    def hide_sidebar(self):
        
        Window.size = (300, 600)
        
        # the backgound is lost so redraw it
        with self.canvas.before:
            self.ids.backgound = Rectangle(pos=(0, 0), size=(300,600), source='data/background_tunnel.jpg')
            
        self.ids.menu.hide()
        self.ids.menu_button.turn_off()
        self.ids.button_all_vpns.turn_off()
            
    def show_screen_logs_openvpn(self):
        self.ids.sm_logs.current = 'screen_logs_openvpn'
        
    def show_screen_logs_blackvpn(self):
        self.ids.sm_logs.current = 'screen_logs_blackvpn'
        
    def show_screen_logs_crash(self):
        self.ids.sm_logs.current = 'screen_logs_crash'
        
    def hide_vpn_speeds(self):
        self.ids.label_vpn_speed_up.text = ''
        self.ids.label_vpn_speed_down.text = ''
        
    def hide_connected_time(self):
        if self._connected_time_event is not None:
            self._connected_time_event.cancel()
            self._connected_time_start = None
            App.get_running_app().setting_vpn_connect_time_updated(None)
        self.ids.label_vpn_timer.text = ''
        
    def reset_connected_time(self):
        if self._connected_time_start is None:
            self._connected_time_start = datetime.datetime.utcnow()
            time_string = self._connected_time_start.strftime("%Y-%m-%d %H:%M:%S")
            App.get_running_app().setting_vpn_connect_time_updated(time_string)

        # update the timer every second via scheduled callback
        self._connected_time_event = Clock.schedule_interval(self.refresh_connected_time, 1)
        
    def set_connected_time(self, datetime_string):
        self._connected_time_start = datetime.datetime.strptime(str(datetime_string), '%Y-%m-%d %H:%M:%S')
        
        # reset_connected_time() will get called later if we are already connected
        
    def refresh_connected_time(self, deltatime):
        if self._connected_time_start is not None:
            connect_time_delta = datetime.datetime.utcnow() - self._connected_time_start
            s = int(connect_time_delta.total_seconds())
            self.ids.label_vpn_timer.text = '{:02}:{:02}:{:02}'.format(s // 3600, s % 3600 // 60, s % 60)

    def reset_speed_up(self, timedelta):
        
        # do not reset if already hidden
        if self.ids.label_vpn_speed_up.text != '':
            self.ids.label_vpn_speed_up.text = '0.00\nKbps'
        
    def reset_speed_down(self, timedelta):
        # do not reset if already hidden
        if self.ids.label_vpn_speed_down.text != '':
            self.ids.label_vpn_speed_down.text = '0.00\nKbps'
        
    def update_label_vpn_speed_up(self, speed):
        self.ids.label_vpn_speed_up.text = speed

        # cancel existing event to reset the spped - since we have a new update
        if self._speed_up_reset_event is not None:
            self._speed_up_reset_event.cancel()
        #reset speed up if its not updated within 3 seconds
        self._speed_up_reset_event = Clock.schedule_once(self.reset_speed_up, 3)
        
    def update_label_vpn_speed_down(self, speed):
        self.ids.label_vpn_speed_down.text = speed
        
        # cancel existing event to reset the spped - since we have a new update
        if self._speed_down_reset_event is not None:
            self._speed_down_reset_event.cancel()
            
        #reset speed down if its not updated within 3 seconds
        self._speed_down_reset_event = Clock.schedule_once(self.reset_speed_down, 3)
        
    ########################################################
    #   Notifications
    ########################################################
        
    def update_setting_show_notifications(self, show_option):
        self._setting_show_notifications = show_option
        
    # this needs to be run in its own thread
    def show_notification_connected(self, *args):
        
        if self._setting_show_notifications == False:
            return
             
        App.get_running_app().systray.show_popup('blackVPN', '\nVPN Connected')
        
    # this needs to be run in its own thread
    def show_notification_disconnected(self, *args):
                
        if self._setting_show_notifications == False:
            return
        
        App.get_running_app().systray.show_popup('blackVPN', '\nVPN Disconnected')
        

        
class WindowsApp(App):
    
    vpncontrol = None
    systray = None
        
    _debug_log = None
        
    _last_vpn_location_id = 'NL'
    _vpn_favorites = []
    
    _update_payload_filename = ''
    _update_payload_url = ''
    _update_payload_md5 = ''
    _update_payload_download_thread = threading.Event()

    _windows_platform = 'unknown'
    
    _first_update_vpn_status = True
    _systray_only_mode = False
    _killswitch = None

    # this queue can be used to send GUI related events to update something from other thread
    # so we can do GUI updates async
    _kivy_events_queue = None
    
    def build(self):
        #super(WindowsApp, self).__init__(**kwargs)
        self.root = root = WindowsWidget()
        
        self.title = "BlackVPN"
        self.icon = "app.ico"
        
        Window.borderless = False
        
        # init the GUI
        self.root.initialize()
        self.load_settings()
        self.root.ids.version.text = 'v ' + str(APP_VERSION)

        self._kivy_events_queue = KivyQueue(notify_func=self._event_received)
        self.killswitch_initialize()

        # update favorites menu
        for vpn_id in self._vpn_favorites:
            #if vpn_id not in self._vpn_favorites:
            self.vpn_favorite_add(vpn_id)
        self.root.ids.menu_favs.hide()
        self.root.ids.hearts_button.turn_off()
        
        # restore the last VPN location used
        self.root.ids.image_background_vpn.source = self.get_vpn_background(self._last_vpn_location_id)
        self.root.ids.label_vpn_location.text = self.get_vpn_name(self._last_vpn_location_id)
        
        # check if we are already connected on UDP or TCP
        Clock.schedule_once(self.vpncontrol.check_connections, 0)
        Clock.schedule_once(self.auto_check_for_app_updates, APP_UPDATES_AUTO_CHECK_DELAY)
        
        Window.bind(on_request_close=self.on_request_close)

        self._windows_platform = platform.platform()
        self.log(LOG_INFO, "Windows Platform is: " + self._windows_platform)
        
        return root

    def on_request_close(self, *args):
        self.log(LOG_INFO, "Window close requested. Hide it to system tray.")
        self.hide()
        self._systray_only_mode = True
        self.systray.show_popup('blackVPN', '\nblackVPN hidden to tray')
        return True

    def killswitch_initialize(self):
        self._killswitch = KillSwitch(self.log)

        # cache killswitch icons, icon will not be shown without it
        self._kivy_events_queue.put('icon_update', 'amber')
        self._kivy_events_queue.put('icon_update', 'green')
        self._kivy_events_queue.put('icon_update', 'off')

        # subscribe to killswitch events for updating killswitch icon
        self._killswitch.killswitch_status_updating.append(lambda: self.update_killswitch_icon_status(updating=True))
        self._killswitch.killswitch_status_updated.append(self.update_killswitch_icon_status)

        Clock.schedule_once(self.save_firewall_settings)
        self.restore_enable_kill_switch_setting()
        self.restore_allow_local_traffic_setting()

    def _event_received(self):
        e = self._kivy_events_queue.get()
        if not e:
            return
        key, value = e
        self.log(LOG_DEBUG, 'Event received: {} / {}'.format(key, value))

        if key == 'icon_update':
            if value == 'amber':
                self.root.ids.icon_killswitch.turn_amber()
            elif value == 'off':
                self.root.ids.icon_killswitch.turn_off()
            elif value == 'green':
                self.root.ids.icon_killswitch.turn_green()

    def update_killswitch_icon_status(self, updating=False):
        """
        We cannot update icon in this function as it's called from another thread.
        So we use events queue for interthread communication.
        """
        if updating:
            self._kivy_events_queue.put('icon_update', 'amber')
        elif self._killswitch.is_kill_switch_enabled():
            self._kivy_events_queue.put('icon_update', 'green')
        else:
            self._kivy_events_queue.put('icon_update', 'off')

    def restore_enable_kill_switch_setting(self):
        setting_enable_kill_switch = keyring.get_password("blackvpn", "setting_enable_kill_switch")
        if setting_enable_kill_switch is not None:
            self.log(LOG_DEBUG, "loaded setting enable kill switch: " + str(setting_enable_kill_switch))
            self.root.ids.setting_enable_kill_switch.active = setting_enable_kill_switch == 'True'
        else:
            self.root.ids.setting_enable_kill_switch.active = False

    def restore_allow_local_traffic_setting(self):
        setting_allow_local_traffic = keyring.get_password("blackvpn", "setting_allow_local_traffic")
        if setting_allow_local_traffic is not None:
            self.log(LOG_DEBUG, "loaded setting enable local traffic: " + str(setting_allow_local_traffic))
            self.root.ids.setting_allow_local_traffic.active = setting_allow_local_traffic == 'True'
        else:
            self.root.ids.setting_allow_local_traffic.active = False

    def save_firewall_settings(self, dt):
        """
        We need to save firewall settings one time at least to be able to use killswitch feature.
        So we save it if we can, and if we cannot and backup file does not exist, we schedule saving again in 10 sec.
        """
        if not os.path.exists(self._killswitch.firewall_backup_filepath):
            if self._killswitch.is_system_ready_for_firewall_settings_backup():
                self._killswitch.save_firewall_settings()
            if not os.path.exists(self._killswitch.firewall_backup_filepath):
                self.set_killswitch_state(False)
                Clock.schedule_once(self.save_firewall_settings, 10)
                return
        self.set_killswitch_state(True)

    def set_killswitch_state(self, enabled):
        """
        State means checkboxes enabled or disabled.
        They should be disabled if firewall restore file does not exist.
        """
        self.root.ids.setting_enable_kill_switch.disabled = not enabled
        self.root.ids.setting_enable_kill_switch_label.disabled = not enabled
        self.set_local_traffic_state(self.root.ids.setting_enable_kill_switch.active and enabled)
        if enabled:
            self.change_system_kill_switch_state_if_necessary()

    def set_local_traffic_state(self, enabled):
        self.root.ids.setting_allow_local_traffic.disabled = not enabled
        self.root.ids.setting_allow_local_traffic_label.disabled = not enabled

    def set_system_tray(self, systray):
        self.systray = systray
        self.systray.set_running_app(self)
        self.systray.set_all_vpns_list(VPN_GLOBAL_LIST)
        
    def set_vpn_control(self, vpncontrol):
        self.vpncontrol = vpncontrol
        # init the VPN control
        self.vpncontrol.set_running_app(self)
    
    def set_debug_log(self, debug_log):
        self._debug_log = debug_log
    
    def hide(self):
        Window.hide()
        
    def restore_app_window(self):
        if not self._systray_only_mode:
            return False
        self.log(LOG_INFO, "Restore window from system tray.")  
        Window.show()
        return True        
    
    def kill(self):
        Clock.schedule_once(self.do_kill)
    
    def do_kill(self, deltatime):
        self.log(LOG_DEBUG, "in do_kill()")
        try:            
            pass
            #self.log(LOG_INFO, "Disconnecting VPN...")
            #self.vpncontrol.vpn_disconnect()
            
            #try:
            #    self.log(LOG_INFO,"stopping Twisted reactor...")
            #    reactor.stop()
                #reactor.callFromThread(reactor.stop)
            #    uninstall_twisted_reactor()
            #except Empty:
            #    self.log(LOG_DEBUG, "caught Twisted empty queue crash on uninstall")
            
            # kill the systray outside the app now
            #self.log(LOG_INFO, "killing the systray now...")
            #self.systray.kill()
        except KeyboardInterrupt:
            raise
        except:
            e = sys.exc_info()[0]
            stacktrace = traceback.format_exc()
            self.log(LOG_ERROR, str(e))
            self.log(LOG_ERROR, str(stacktrace))

        self._debug_log.close()
        stopTouchApp()
        print "Thanks. Bye."
    
    def log(self, status, message):
        
        # add newline if needed
        #if message.endswith('\n') == False and message.endswith('\r') == False: 
        #    message += '\n'

        log_prepend = ''
        if status == LOG_DEBUG:
            log_prepend = '[  ---  ] '
        if status == LOG_INFO:
            log_prepend = '[  info ] '
        if status == LOG_WARN:
            log_prepend = '[--warn-] '
        if status == LOG_ERROR:
            log_prepend = '[=error=] '
        if status == LOG_CRITICAL:
            log_prepend = '[==FAIL=] '
        if status == LOG_VPN:
            log_prepend = '[openvpn] '
        
        log_message = log_prepend + datetime.datetime.utcnow().isoformat("T") + ': ' + str(message)
        
        # print debug to console
        if status >= LOG_DEBUG: 
            print log_message
        
        # store everything in debug log
        try:
            if self._debug_log is not None:
                self._debug_log.write(log_message + '\n')
        except: 
            print "FAIL trying to write to the debug log."
        
    def on_stop(self):
        # The Kivy event loop is about to stop, set a stop signal;
        # otherwise the app window will close, but the Python process will
        # keep running until all secondary threads exit.
        self._update_payload_download_thread.set()
        
        # no more stuff can be done - the app is being shutdown already

    def on_popup_yes(self):
        # routine for going yes
        pass

    def text_popup(self, title='', text='', ok_callback=None):
        """
        Open the pop-up with the name.
        """
        dlg = MessageBox(self, titleheader=title, message=text,
                         options={"OK": ok_callback, "Cancel": ""},
                         size=(self.root.width, 300))

    ########################################################
    #   Favorites
    ########################################################
    
    def vpn_favorite_add(self, vpn_location_id):
        self.log(LOG_DEBUG, "vpn_favorite_add() for " + vpn_location_id)
        
        if vpn_location_id in self._vpn_favorites:
            self.log(LOG_DEBUG, "vpn_favorite_add(): its already in the list. Did NOT add " +  vpn_location_id) 
        else:
            self._vpn_favorites.append(str(vpn_location_id))
            self.log(LOG_DEBUG, "vpn_favorite_add(): its not in the list. Added " +  vpn_location_id + " to list: " + str(self._vpn_favorites))
        
        self.root.ids.menu_favs.hide()
        self.root.ids.menu_button.turn_off()
        self.root.ids.menu.hide()
        
        if vpn_location_id in VPN_GLOBAL_LIST:
            button = BubbleButton(text=self.get_vpn_name(vpn_location_id))
            # an alternative is to use fbind instead of partial. See: https://kivy.org/docs/api-kivy.event.html
            button.bind(on_release=partial(self.vpn_favorite_select, vpn_location_id))
            self.root.ids.menu_favs.add_widget(button)
            self.root.ids.menu_favs.height = (30 * len(self.root.ids.menu_favs.content.children))
            self.root.ids.menu_favs.pos_y -= 30
        else:
            self.log(LOG_ERROR, "ERROR: Could not add favorite VPN with vpn_location_id=" + vpn_location_id)
            return None
        
        self.root.ids.hearts_button.turn_on()
        self.root.ids.menu_favs.show()
        
        self.systray.add_favorite(vpn_location_id)
        self.setting_vpn_favorites_updated()
        

    
    def vpn_favorite_remove(self, vpn_location_id):
        self.log(LOG_DEBUG, "vpn_favorite_remove() for " + vpn_location_id)
        
        if vpn_location_id not in self._vpn_favorites:
            self.log(LOG_ERROR, "vpn_favorite_remove() cannot remove because its not in self._vpn_favorites: " + str(self._vpn_favorites))
        
        self.root.ids.menu_favs.hide()
        self.root.ids.menu_button.turn_off()
        self.root.ids.menu.hide()
        
        vpn_name = self.get_vpn_name(vpn_location_id)
        for button in self.root.ids.menu_favs.content.children:
            if button.text is vpn_name:
                self.root.ids.menu_favs.remove_widget(button)
                self.root.ids.menu_favs.height = (30 * len(self.root.ids.menu_favs.content.children))
                self.root.ids.menu_favs.pos_y += 30
        
        self.root.ids.hearts_button.turn_on()
        self.root.ids.menu_favs.show()
        
        self.systray.remove_favorite(vpn_location_id)
        
        # update settings
        for favorite in self._vpn_favorites:
            if favorite == vpn_location_id:
                self._vpn_favorites.remove(favorite)
                
        self.setting_vpn_favorites_updated()
        
    def vpn_favorite_select(self, *args, **kwargs):
        vpn_id = str(args[0])
        self.do_vpn_connect(vpn_id)

        
    def is_vpn_favorite(self, vpn_location_id):
        if vpn_location_id in self._vpn_favorites:
            return True
        else:
            return False
            
    def get_vpn_name(self, vpn_location_id):
        if vpn_location_id in VPN_GLOBAL_LIST:
            return VPN_GLOBAL_LIST[vpn_location_id]['name']
        else:
            self.log(LOG_ERROR, "ERROR: Could not find info on VPN with vpn_location_id=" + vpn_location_id)
            return None
        
    def get_vpn_background(self, vpn_location_id):
        if vpn_location_id in VPN_GLOBAL_LIST:
            return VPN_GLOBAL_LIST[vpn_location_id]['background']
        else:
            self.log(LOG_ERROR, "ERROR: Could not find info on VPN with vpn_location_id=" + vpn_location_id)
            return None
            
    # called from multiple sources
    # - If user clicks on an option in the favorites menu
    # - if user clicks on a flag in the Global VPNs screen
    # - if user clicks the VPN ON/OFF button 

    def do_vpn_connect(self, vpn_location_id):
        self.log(LOG_DEBUG, "In app.do_vpn_connect() for location: " + str(vpn_location_id))
        try:
            if vpn_location_id in VPN_GLOBAL_LIST:
            
                # store location for later
                if vpn_location_id != self._last_vpn_location_id:
                    self._last_vpn_location_id = vpn_location_id
                    self.setting_last_vpn_location_id_updated()
                    self.systray.set_last_vpn_location_id(vpn_location_id)
                elif self.vpncontrol.status_is_disconnected != True:
                    self.log(LOG_WARN, 'Already connected to location ' + str(vpn_location_id))
                    
            
                # disconnect from current location if already connected
                if self.vpncontrol.status_is_disconnected() != True:
                    self.vpncontrol.vpn_disconnect()
            
                # set the ON/OFF switch
                self.root.ids.button_vpn_switch.set_amber()
            
                # hide all menus and sidebar
                self.root.init_to_connect()
                            
                self.root.ids.image_background_vpn.source = self.get_vpn_background(vpn_location_id)
                self.root.ids.label_vpn_location.text = self.get_vpn_name(vpn_location_id)
                                            
                if self.root.ids.protoStunnel.active == False:
                    self.vpncontrol.set_vpn_location(VPN_GLOBAL_LIST[vpn_location_id]['remote'], vpn_location_id)
                else:                    
                    if self.vpncontrol.get_vpn_remote != STUNNEL_LOCALHOST:
                        self.vpncontrol.set_vpn_location(STUNNEL_LOCALHOST, vpn_location_id)
                        
                    # update the port to a stunnel port if its too low
                    if 'stunnel_ports' in VPN_GLOBAL_LIST[vpn_location_id]:
                        stunnel_ports = VPN_GLOBAL_LIST[vpn_location_id]['stunnel_ports']
                        if len(stunnel_ports) == 0:
                            self.log(LOG_INFO, "sTunnel ports: NONE FOUND")
                            self.vpncontrol.set_vpn_port('443')
                        else:
                            stunnel_port = stunnel_ports[0]
                            self.log(LOG_INFO, "Using sTunnel port: " + str(stunnel_port))
                            if stunnel_port != self.vpncontrol.get_vpn_port():
                                self.vpncontrol.set_vpn_port(stunnel_port)
                                keyring.set_password("blackvpn", "last_stunnel_port", stunnel_port)
                
                # Start the VPN connection process
                Clock.schedule_once(self.vpncontrol.vpn_connect, 1)

            else:
                self.log(LOG_ERROR, "ERROR: Could not find info on VPN with vpn_location_id=" + vpn_location_id)
                return None
        except KeyboardInterrupt:
            raise
        except:
            e = sys.exc_info()[0]
            stacktrace =  traceback.format_exc()
            self.log(LOG_ERROR, 'Failed in do_vpn_connect()\n' + str(e))
            self.log(LOG_ERROR, str(stacktrace))
        
    ########################################################
    #   Callbacks from VpnControl class
    ########################################################
    
    def update_label_vpn_speed_up(self, speed):
        self.root.update_label_vpn_speed_up(speed)
        
    def update_label_vpn_speed_down(self, speed):
        self.root.update_label_vpn_speed_down(speed)
        
    def update_notification_alerts(self, colour, message):
        self.log(LOG_DEBUG, "Updating alerts notification with message=" + message)
        self.root.update_notification_alerts(colour, message)
        
    def do_auto_connect(self, deltatime):
        if self.root.ids.setting_vpn_auto_connect.active == True:
            self.log(LOG_INFO, "VPN is auto-connecting to the last location.")
            self.on_vpn_switch_clicked()
        else:
            self.log(LOG_INFO, "NOT auto-connecting to the last location (user setting).")
    
    def restart_vpn_service_failed(self):
        self.root.ids.support_restart_msg.text = "[b][color=cc4444]Restart failed![/color][/b]"
        self.root.ids.support_vpn_restart_button.set_off()
        
    def restart_vpn_service_suceeded(self):
        self.root.ids.support_restart_msg.text = "[b][color=44cc44]Restart OK[/color][/b]"
        self.root.ids.support_vpn_restart_button.set_on()
        
    def update_vpn_status(self, vpn_status):
    
        # use this to do not show "connected" popup first time when app started and VPN connected already
        do_not_show_connected_popup = self._first_update_vpn_status
        self._first_update_vpn_status = False

        # update progress bar
        self.root.ids.vpn_progress_bar.value = int(vpn_status)

        if vpn_status > VPN_STATUS_DISCONNECTED:
            self.change_system_kill_switch_state_if_necessary()

        if vpn_status == VPN_STATUS_DISCONNECTED:
            self.root.init_disconnected()
            self.systray.setIconRed()
            self.root.ids.icon_dnsleak.turn_off()
        elif VPN_STATUS_DISCONNECTED < vpn_status < VPN_STATUS_CONNECTED:
            self.root.init_connecting()
            self.root.reset_notification_alerts()
            self.systray.setIconAmber()
            
            if vpn_status >= VPN_STATUS_BLOCK_DNS:
                self.root.ids.icon_dnsleak.turn_green()
            else:
                self.root.ids.icon_dnsleak.turn_amber()
        elif vpn_status == VPN_STATUS_CONNECTED:
            
            self.root.init_connected(self.vpncontrol.get_vpn_location_id(), self.vpncontrol.get_vpn_protocol(),
                                     self.vpncontrol.get_vpn_port(), self.vpncontrol.get_vpn_remote(),
                                     do_not_show_connected_popup)
            self.root.update_notification_p2p(VPN_GLOBAL_LIST[self.vpncontrol.get_vpn_location_id()]['p2p'])
            self.systray.setIconGreen()
            self.root.ids.icon_dnsleak.turn_green()
        else:
            self.log(LOG_ERROR, "Unknown VPN status: " + str(vpn_status))
            
    ########################################################
    #   GUI Vibes
    ########################################################
        
    def on_vpn_switch_clicked(self):
        
        if self.root.ids.button_vpn_switch.is_off():
            self.root.ids.button_vpn_switch.set_on()
            self.do_vpn_connect(self._last_vpn_location_id)
            
        elif self.root.ids.button_vpn_switch.is_amber() or self.root.ids.button_vpn_switch.is_on():
            if self.root.ids.setting_enable_kill_switch.active:
                self.text_popup(title='Kill Switch warning',
                                text='Kill Switch will be turned Off'
                                     '\nso your system can be compromised.'
                                     '\nAre you sure you want to continue?',
                                ok_callback=self.disable_kill_switch_and_disconnect)
            else:
                self.disconnect()

        else:
            self.log(LOG_ERROR, "unknown state for button_vpn_switch")

    def disable_kill_switch_and_disconnect(self):
        self.disconnect()
        self.disable_kill_switch()

    def disconnect(self):
        self.root.ids.button_vpn_switch.set_off()
        self.vpncontrol.vpn_disconnect()

    def on_press_button_menu(self):
        self.root.ids.menu_button.show_on()
        
        
    def on_release_button_menu(self):
        if self.root.ids.menu_button.is_on():
            self.root.ids.menu_button.turn_off()
            self.root.ids.menu.hide()
            self.root.hide_sidebar()
        else:
            self.root.ids.menu_button.turn_on()
            self.root.ids.menu.show()
            
            #self.root.ids.button_all_vpns.turn_off()
            self.root.ids.hearts_button.turn_off()
            self.root.ids.menu_favs.hide()
        
    def on_press_button_all_vpns(self):
        self.root.ids.button_all_vpns.show_on()
        
    def on_release_button_all_vpns(self):
        if self.root.ids.button_all_vpns.is_on():
            self.root.ids.button_all_vpns.turn_off()
            self.root.hide_sidebar()
        else:
            self.root.ids.button_all_vpns.turn_on()
            
            self.root.ids.menu_button.turn_off()
            self.root.ids.menu.hide()
            self.root.ids.hearts_button.turn_off()
            self.root.ids.menu_favs.hide()
            
            self.root.ids.sm.current = 'screen_global'
            self.root.show_sidebar()


    def on_press_button_hearts(self):
        self.root.ids.hearts_button.show_on()
        
        
        
    def on_release_button_hearts(self):
        if self.root.ids.hearts_button.is_on():
            self.root.ids.hearts_button.turn_off()
            self.root.ids.menu_favs.hide()
        else:
            self.root.ids.hearts_button.turn_on()
            self.root.ids.menu_favs.show()
            
            #self.root.ids.button_all_vpns.turn_off()
            self.root.ids.menu_button.turn_off()
            self.root.ids.menu.hide()
            
    def on_press_button_hide(self):
        pass
        
    def on_release_button_hide(self):
        Window.hide()
        
    def on_release_button_hide_sidebar(self):
        self.root.hide_sidebar()

        
    def on_release_icon_killswitch(self):
        
        if self.root.ids.bubble_killswitch.is_hidden():
            self.root.ids.bubble_killswitch.show()
            self.root.ids.bubble_dnsleak.hide()
            self.root.ids.bubble_p2p.hide()
            self.root.ids.bubble_alerts.hide()
        else:
            self.root.ids.bubble_killswitch.hide()

    def on_release_icon_dnsleak(self):
        
        if self.root.ids.bubble_dnsleak.is_hidden():
            self.root.ids.bubble_dnsleak.show()
            self.root.ids.bubble_killswitch.hide()
            self.root.ids.bubble_p2p.hide()
            self.root.ids.bubble_alerts.hide()
        else:
            self.root.ids.bubble_dnsleak.hide()
    
    def on_release_icon_p2p(self):
        
        if self.root.ids.bubble_p2p.is_hidden() and self.root.ids.p2p_bubble_button.text != '':
            self.root.ids.bubble_p2p.show()
            self.root.ids.bubble_killswitch.hide()
            self.root.ids.bubble_dnsleak.hide()
            self.root.ids.bubble_alerts.hide()
        else:
            self.root.ids.bubble_p2p.hide()
            
    def on_release_icon_alerts(self):
        
        if self.root.ids.bubble_alerts.is_hidden() and self.root.ids.alerts_bubble_button.text != '':
            self.root.ids.bubble_alerts.show()
            self.root.ids.bubble_killswitch.hide()
            self.root.ids.bubble_dnsleak.hide()
            self.root.ids.bubble_p2p.hide()
        else:
            self.root.ids.bubble_alerts.hide()
        
    def on_release_menu_vpn_settings(self):
        self.root.show_sidebar()
        self.root.ids.sm.current = 'screen_settings_vpn'
                
    def on_release_menu_logs(self):
        self.root.show_sidebar()
        self.root.ids.sm.current = 'screen_logs'
        
    def on_release_menu_updates(self):
        self.root.show_sidebar()
        self.root.ids.sm.current = 'screen_updates'
        
    def on_release_menu_support(self):
        self.root.show_sidebar()
        self.root.ids.sm.current = 'screen_support'
        
    def on_release_menu_quit(self):
        self.kill()
        
    def auto_check_for_app_updates(self, deltatime):
        
        if self.root.ids.button_check_app_updates.is_on():
            self.log(LOG_DEBUG, "auto-checking for app updates...")
            self.do_check_for_app_updates()
        else:
            self.log(LOG_DEBUG, "Not auto-checking for updates - process already happening.")
        
    def do_check_for_app_updates(self):
        self.log(LOG_DEBUG, "Requesting app update info from: " + APP_UPDATES_URL)
        self.root.ids.app_updates_check_message.text = "Checking for updates..."
        self.root.ids.app_updates_check_result.text = ''
        self.root.ids.button_check_app_updates.turn_off()
        
        headers = {'Accept': 'application/json'}
        app_update_request = UrlRequest(APP_UPDATES_URL, 
                                    on_success=self.on_app_updates_check_success, 
                                    on_failure=self.on_app_updates_check_failure, 
                                    on_error=self.on_app_updates_check_error, 
                                    #req_body=json.dumps(support_ticket_data),
                                    req_headers=headers,
                                    decode=False)

    def to_str(self, message):
        """
        Return ascii symbols only, to make sure kivy will not crash on symbols that it cannot convert
        Code from https://stackoverflow.com/a/35579848/3477220

        TODO: decode the string correctly
        """
        try:
            return re.sub(r'[^\x00-\x7f]', r'', str(message))
        except UnicodeDecodeError:
            return "Unable to decode result string..."

    def on_app_updates_check_failure(self, request, result):
        self.log(LOG_ERROR, 'on_app_updates_check_failure(): error: ' + str(request.error))
        self.log(LOG_ERROR, 'on_app_updates_check_failure(): result: ' + str(request.result))
        
        self.root.ids.app_updates_check_result.text = self.to_str(request.error)
        
    def on_app_updates_check_error(self, request, result):

        self.log(LOG_ERROR, 'on_app_updates_check_error(): error: ' + str(request.error))
        self.log(LOG_ERROR, 'on_app_updates_check_error(): result: ' + str(request.result))

        self.root.ids.app_updates_check_result.text = self.to_str(request.error)

    def on_app_updates_check_success(self, request, result):
        self.log(LOG_INFO, 'Update info fetched: ' + self.to_str(request.result))
        
        try:
            update_list = json.loads(self.to_str(request.result))
            self.log(LOG_INFO, 'Number of updates fetched: ' + str(len(update_list)))
            
            last_update = update_list[-1]
            
            self.log(LOG_INFO, 'update_number: ' + str(last_update['update_number']))
            self.log(LOG_INFO, 'update_type: ' + str(last_update['update_type']))
            self.log(LOG_INFO, 'update_url: ' + str(last_update['update_url']))
            self.log(LOG_INFO, 'update_md5: ' + str(last_update['update_md5']))
            self.log(LOG_INFO, 'update_version: ' + str(last_update['update_version']))
            self.log(LOG_INFO, 'update_text: ' + str(last_update['update_text']))
            
            #only update if its a new version available
            if float(last_update['update_version']) > APP_VERSION:
                
                # show updates screen
                self.root.show_sidebar()
                self.root.ids.sm.current = 'screen_updates'
                self.root.ids.button_install_app_updates.turn_on()
                self.on_release_menu_updates()
            
                update_payload_url = last_update['update_url']
                update_payload_filename = APP_UPDATES_EXE_FILENAME
                if last_update['update_type'] == 'zip':
                    update_payload_filename = APP_UPDATES_ZIP_FILENAME
                
                self._update_payload_filename = join(tempfile.gettempdir(), update_payload_filename)
                self._update_payload_url = last_update['update_url']
                self._update_payload_md5 = last_update['update_md5']
                                
                self.root.ids.app_updates_check_result.text = 'Found update v' + str(last_update['update_version'])
                self.root.ids.app_updates_check_message.text = str(last_update['update_text'])
                self.log(LOG_INFO, "Found update #" + str(last_update['update_number']) + "from URL: " + update_payload_url + " as " + update_payload_filename)
                

            else:
                self.root.ids.app_updates_check_result.text = 'Already running the latest version.'
                self.root.ids.app_updates_check_message.text = "No updates."
                self.log(LOG_INFO, 'Already running the latest version. Skipping update.')
                Clock.schedule_once(self.reset_app_updates_page, 30)
        except:
            e = sys.exc_info()[0]
            stacktrace =  traceback.format_exc()
            self.log(LOG_ERROR, "Error processing app updates JSON: " + str(stacktrace ))       
                
    def reset_app_updates_page(self, deltatime):
        self.root.ids.app_updates_check_message.text = 'Click the toolbox button\nto check for updates now.'
        self.root.ids.app_updates_check_result.text = ''
        self.root.ids.button_check_app_updates.turn_on()
        self.root.ids.button_install_app_updates.turn_off()
        
    
    ########################################################
    #   Settings
    ########################################################
    
    def load_settings(self):
        username = keyring.get_password("blackvpn", "username")
        password =  keyring.get_password("blackvpn", "password")
        if username is not None:
            self.log(LOG_DEBUG, "loaded setting username: " + username)      
            if password is not None:
                self.log(LOG_DEBUG, "loaded setting password: ************")
                self.root.ids.setting_vpn_username.text = username
                self.root.ids.setting_vpn_password.text = password
                
                self.vpncontrol.set_vpn_username(username)
                self.vpncontrol.set_vpn_password(password)
    
        if username is None or len(username) < 8 or password is None or len(password) < 8:
            self.log(LOG_WARN, "invalid username:" + str(username) + " or password:" + str(password))
            # show the settings page
            self.on_release_menu_vpn_settings()
            self.root.ids.bubble_username_password.show()
        else:
            self.root.ids.bubble_username_password.hide()
    
        last_vpn_location = keyring.get_password("blackvpn", "last_vpn_location_id")
        if last_vpn_location is not None:
            self._last_vpn_location_id = last_vpn_location
            self.log(LOG_DEBUG, "loaded setting last location: " + last_vpn_location)
            self.systray.set_last_vpn_location_id(last_vpn_location)
            self.vpncontrol.set_vpn_location(VPN_GLOBAL_LIST[last_vpn_location]['remote'], last_vpn_location)
        
        vpn_favorites = keyring.get_password("blackvpn", "vpn_favorites")
        if vpn_favorites is not None and len(vpn_favorites) > 0:
            vpn_favorites = vpn_favorites.split(':')

            # remove any duplicates
            for vpn in vpn_favorites:
              if vpn not in self._vpn_favorites:
                self._vpn_favorites.append(vpn)
            
            self.log(LOG_DEBUG, "loaded setting favorites: " + str(self._vpn_favorites))
            
        auto_connect = keyring.get_password("blackvpn", "autoconnect")
        if auto_connect is not None:
            self.log(LOG_DEBUG, "loaded setting auto-connect: " + auto_connect)
            if auto_connect == 'True':
                self.root.ids.setting_vpn_auto_connect.active = True
            else:
                self.root.ids.setting_vpn_auto_connect.active = False
        
        last_connected_time = keyring.get_password("blackvpn", "last_connected_time")
        if last_connected_time is not None:
            self.log(LOG_DEBUG, "loaded setting last_connected_time: " + last_connected_time)
            self.root.set_connected_time(last_connected_time)
        
        setting_show_notifications = keyring.get_password("blackvpn", "setting_show_notifications")
        if setting_show_notifications is not None:
            self.log(LOG_DEBUG, "loaded setting show notifications: " + str(setting_show_notifications))
            if setting_show_notifications == 'True':
                self.root.update_setting_show_notifications(True)
                self.root.ids.setting_show_notifications.active = True
            else:
                self.root.update_setting_show_notifications(False)
                self.root.ids.setting_show_notifications.active = False
        else:
            self.root.update_setting_show_notifications(False)
            self.root.ids.setting_show_notifications.active = False
        
        vpn_crypto = keyring.get_password("blackvpn", "vpn_crypto")
        if vpn_crypto is not None:
            self.log(LOG_DEBUG, "loaded setting vpn_crypto: " + vpn_crypto)
            self.vpncontrol.set_vpn_crypto(vpn_crypto)
            
            if vpn_crypto == "AES-256-GCM":
                self.root.ids.aes256gcm.active = True
                self.root.ids.aes256cbc.active = False
            else:
                self.root.ids.aes256gcm.active = False
                self.root.ids.aes256cbc.active = True
        
        last_app_update_number = keyring.get_password("blackvpn", "last_app_update_number")
        if last_app_update_number is not None:
            self.log(LOG_DEBUG, "loaded setting last_app_update: " + last_app_update_number)
            self._last_app_update_number = int(last_app_update_number)

        vpn_protocol = keyring.get_password("blackvpn", "last_vpn_protocol")
        if vpn_protocol is not None:
            self.log(LOG_DEBUG, "loaded setting last protocol: " + vpn_protocol)
            if vpn_protocol == "protoUDP443":
                self.root.ids.protoUDP443.active = True
                self.root.ids.protoUDP1194.active = False
                self.root.ids.protoTCP443.active = False
                self.root.ids.protoStunnel.active = False
                self.vpncontrol.set_vpn_port('443')
                self.vpncontrol.set_vpn_protocol('udp')
                
            elif vpn_protocol == "protoUDP1194":
                self.root.ids.protoUDP1194.active = True
                self.root.ids.protoUDP443.active = False
                self.root.ids.protoTCP443.active = False
                self.root.ids.protoStunnel.active = False
                self.vpncontrol.set_vpn_port('1194')
                self.vpncontrol.set_vpn_protocol('udp')
                
            elif vpn_protocol == "protoTCP443":
                self.root.ids.protoTCP443.active = True
                self.root.ids.protoUDP443.active = False
                self.root.ids.protoUDP1194.active = False
                self.root.ids.protoStunnel.active = False
                self.vpncontrol.set_vpn_port('443')
                self.vpncontrol.set_vpn_protocol('tcp')
                
            elif vpn_protocol == "protoStunnel":
                self.root.ids.protoTCP443.active = False
                self.root.ids.protoUDP443.active = False
                self.root.ids.protoUDP1194.active = False
                self.root.ids.protoStunnel.active = True
                self.vpncontrol.set_vpn_protocol('tcp')
                self.vpncontrol.set_vpn_stunnel_ports(VPN_GLOBAL_LIST[self._last_vpn_location_id]['stunnel_ports'])
                
                last_stunnel_port = keyring.get_password("blackvpn", "last_stunnel_port")
                if last_stunnel_port is not None:
                    self.log(LOG_DEBUG, "loaded setting last stunnel port: " + last_stunnel_port)
                    self.vpncontrol.set_vpn_port(int(last_stunnel_port))
        
    def setting_vpn_favorites_updated(self):
        favorites = ':'.join(self._vpn_favorites)
        keyring.set_password("blackvpn", "vpn_favorites", favorites)
        self.log(LOG_INFO, "VPN favorites updated to: " + favorites)
        
    def setting_vpn_auto_connect_updated(self):
        self.log(LOG_INFO, "VPN auto-connect is updated to: " + str(self.root.ids.setting_vpn_auto_connect.active))
        keyring.set_password("blackvpn", "autoconnect", str(self.root.ids.setting_vpn_auto_connect.active))
    
    def last_app_update_number_updated(self, last_app_update_number):
        self.log(LOG_INFO, "VPN auto-connect is updated to: " + str(last_app_update_number))
        keyring.set_password("blackvpn", "last_app_update_number", str(last_app_update_number))
        self._last_app_update_number = int(last_app_update_number)
    
    def setting_vpn_connect_time_updated(self, connect_time):
        if connect_time is not None:
            self.log(LOG_INFO, "VPN last connection time is updated to: " + str(connect_time))
            keyring.set_password("blackvpn", "last_connected_time", str(connect_time))
        else:
            try:
                self.log(LOG_INFO, "VPN last connection time is being deleted.")
                keyring.delete_password("blackvpn", "last_connected_time")
            except KeyboardInterrupt:
                raise
            except:
                pass
    
    def setting_show_notifications_updated(self):
        self.log(LOG_INFO, "setting show notifications is updated to: " + str(self.root.ids.setting_show_notifications.active))
        self.root.update_setting_show_notifications(self.root.ids.setting_show_notifications.active)
        keyring.set_password("blackvpn", "setting_show_notifications", str(self.root.ids.setting_show_notifications.active))

    def change_system_kill_switch_state_if_necessary(self):
        """
        Do nothing if VPN status is Off.
        Otherwise try to apply kill switch if checkbox is On.
        """
        if self.root.ids.button_vpn_switch.is_off():
            return

        if self.root.ids.setting_enable_kill_switch.active:
            self.enable_kill_switch()
        else:
            self.disable_kill_switch()

    def enable_kill_switch(self):
        self.log(LOG_INFO, "check killswitch enabled")
        self._killswitch.enable_kill_switch()
        self.change_system_allow_local_traffic_state_if_necessary()

    def disable_kill_switch(self):
        self.log(LOG_INFO, "check killswitch disabled")
        self._killswitch.disable_kill_switch()

    def change_system_allow_local_traffic_state_if_necessary(self):
        if self.root.ids.setting_allow_local_traffic.active:
            self._killswitch.allow_local_traffic()
        else:
            self._killswitch.block_local_traffic()

    def setting_enable_kill_switch_updated(self):
        self.change_system_kill_switch_state_if_necessary()

        kill_switch_checkbox_enabled = self.root.ids.setting_enable_kill_switch.active
        keyring.set_password("blackvpn", "setting_enable_kill_switch",
                             kill_switch_checkbox_enabled)
        self.set_local_traffic_state(kill_switch_checkbox_enabled)

    def setting_allow_local_traffic_updated(self):
        self.change_system_allow_local_traffic_state_if_necessary()
        keyring.set_password("blackvpn", "setting_allow_local_traffic",
                             self.root.ids.setting_allow_local_traffic.active)

    def setting_last_vpn_location_id_updated(self):
        self.log(LOG_INFO, "VPN last location ID updated to: " + str(self._last_vpn_location_id))
        keyring.set_password("blackvpn", "last_vpn_location_id", self._last_vpn_location_id)
    
    def setting_vpn_crypto_updated(self):
        if self.root.ids.aes256cbc.active == True:
            self.vpncontrol.set_vpn_crypto('AES-256-CBC')
            keyring.set_password("blackvpn", "vpn_crypto", 'AES-256-CBC')
            self.log(LOG_INFO, "VPN crypto changed to: AES-256-CBC")

        elif self.root.ids.aes256gcm.active == True:
            self.vpncontrol.set_vpn_crypto('AES-256-GCM')
            keyring.set_password("blackvpn", "vpn_crypto", 'AES-256-GCM')
            self.log(LOG_INFO, "VPN crypto changed to: AES-256-GCM")
        
    
    def setting_vpn_username_updated(self):
        username = self.root.ids.setting_vpn_username.text
        self.log(LOG_INFO, "VPN username updated to: " + str(username))
        
        keyring.set_password("blackvpn", "username", username)
        self.vpncontrol.set_vpn_username(username)
        
        if username is None or len(username) < 8 or len(username) > 12:
            self.log(LOG_WARN, "invalid username:" + str(username))
            self.root.ids.bubble_username_password.show()
        else:
            self.root.ids.bubble_username_password.hide()
        
    def setting_vpn_password_updated(self):
        password = self.root.ids.setting_vpn_password.text
        self.log(LOG_INFO, "VPN password updated.")
        
        keyring.set_password("blackvpn", "password", password)
        self.vpncontrol.set_vpn_password(password)
        
        if password is None or len(password) < 8 or len(password) > 20:
            self.log(LOG_WARN, "invalid password length: {}".format(len(password)))
            self.root.ids.bubble_username_password.show()
        else:
            self.root.ids.bubble_username_password.hide()
    
    def setting_vpn_protocol_updated(self):
        #setup ports + protocols based on GUI selector
        if self.root.ids.protoUDP443.active == True:
            self.vpncontrol.set_vpn_port('443')
            self.vpncontrol.set_vpn_protocol('udp')
            keyring.set_password("blackvpn", "last_vpn_protocol", 'protoUDP443')
            self.log(LOG_INFO, "VPN protcol changed to: UDP/443")

        elif self.root.ids.protoUDP1194.active == True:
            self.vpncontrol.set_vpn_port('1194')
            self.vpncontrol.set_vpn_protocol('udp')
            keyring.set_password("blackvpn", "last_vpn_protocol", 'protoUDP1194')
            self.log(LOG_INFO, "VPN protcol changed to: UDP/1194")
            
        elif self.root.ids.protoTCP443.active == True:
            self.vpncontrol.set_vpn_port('443')
            self.vpncontrol.set_vpn_protocol('tcp')
            keyring.set_password("blackvpn", "last_vpn_protocol", 'protoTCP443')
            self.log(LOG_INFO, "VPN protcol changed to: TCP/443")
            
        elif self.root.ids.protoStunnel.active == True:
            self.log(LOG_INFO, "VPN protcol changed to: sTunnel")
            keyring.set_password("blackvpn", "last_vpn_protocol", 'protoStunnel')
            
            self.vpncontrol.set_vpn_protocol('tcp')
            
            if 'stunnel_ports' in VPN_GLOBAL_LIST[self._last_vpn_location_id]:
                stunnel_ports = VPN_GLOBAL_LIST[self._last_vpn_location_id]['stunnel_ports']
                if len(stunnel_ports) == 0:
                    self.log(LOG_INFO, "sTunnel ports: NONE FOUND")
                    self.vpncontrol.set_vpn_port('443')
                else:
                    self.log(LOG_INFO, "Using sTunnel port: " + str(stunnel_ports[0]))
                    self.vpncontrol.set_vpn_port(stunnel_ports[0])
                    keyring.set_password("blackvpn", "last_stunnel_port", stunnel_ports[0])
        else:
            self.log(LOG_ERROR, "Failed to find which GUI item is selected")
 
    # callback from GUI button: button_install_app_updates
    def do_install_app_updates(self):
        
        # ignore if the button is hidden
        if self.root.ids.button_install_app_updates.is_on() == True:
            self.root.ids.button_install_app_updates.turn_off()
            self.root.ids.app_updates_check_result.text = 'Downloading update to ' + self._update_payload_filename
            self.root.ids.app_updates_check_message.text = "Downloading update..."

            threading.Thread(target=self.download_app_update_and_die).start()
 
    # this should run in a separate thread as: threading.Thread(target=self.download_app_update_and_die).start()
    def download_app_update_and_die(self):
        try:
            self.log(LOG_INFO, "Downloading update payload from: " + self._update_payload_url)
            self.log(LOG_INFO, "Downloading to file: " + self._update_payload_filename)
            
            urllib.urlretrieve(self._update_payload_url, self._update_payload_filename)
            #urllib2.urlopen(self._update_payload_url, join(get_app_path(), self._update_payload_filename))
            
            update_md5 = self.get_md5_for_file(self._update_payload_filename)
            if self._update_payload_md5 is not None and update_md5 != self._update_payload_md5:
                self.root.ids.app_updates_check_result.text = 'Downloaded file md5:' + update_md5 + ' does not equal expected md5:' + self._update_payload_md5
                self.root.ids.app_updates_check_message.text = "Update failed."
                self.log(LOG_ERROR, 'Downloaded file md5:' + update_md5 + ' does not equal expected md5:' + self._update_payload_md5)
            else:
                self.root.ids.app_updates_check_result.text = 'Disconnecting and exiting blackVPN app. Launching updated installer...'
                self.root.ids.app_updates_check_message.text = "Launching installer..."
                self.log(LOG_INFO, "Finished downloading update payload: " + self._update_payload_filename)
                self.vpncontrol.vpn_disconnect()
                Clock.schedule_once(self.launch_update_process_and_die, 5)
        except:
            e = sys.exc_info()[0]
            self.log(LOG_ERROR, traceback.format_exc())
            self.reset_app_updates_page(None)
            
    def launch_update_process_and_die(self, timedelta):
        try:
            flag_create_new_console = 0x00000010
            #updater_filename = join(get_app_path(), APP_UPDATES_EXE_FILENAME)
            pid = subprocess.Popen([self._update_payload_filename], creationflags=flag_create_new_console).pid
            self.kill()
        except KeyboardInterrupt:
            raise
        except:
            e = sys.exc_info()[0]
            stacktrace =  traceback.format_exc()
            self.log(LOG_ERROR, str(e))
            self.log(LOG_ERROR, str(stacktrace))
            
            self.reset_app_updates_page(None)
            
    def get_md5_for_file(self, filename):
        hash_md5 = hashlib.md5()
        with open(filename, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()
        
    def support_vpn_restart_button_pressed(self):
        self.log(LOG_WARN, "VPN restart button pressed.... waiting for release.")
        # Dont do anything if the button is set to off
        if self.root.ids.support_vpn_restart_button.is_off() != True:
            self.root.ids.support_vpn_restart_button.set_amber()
        
    def support_vpn_restart_button_released(self):
        self.log(LOG_WARN, "VPN restart button released.")
        self.vpncontrol.restart_vpn_service()

 
    def support_send_button_pressed(self):
        # Dont do anything if the button is set to off
        if self.root.ids.support_send_button.is_off() != True:
            self.root.ids.support_send_button.set_amber()
        
    def support_send_button_released(self):
        # Dont do anything if the button is set to off
        if self.root.ids.support_send_button.is_off() != True:
            self.root.ids.support_send_button.set_on()
            
            #check that all fields are filled in
            if self.root.ids.support_email.text is None or self.root.ids.support_email.text.find('@') == -1:
                self.root.ids.support_error_msg.text = '[b][color=cc4444]Please enter a valid email address.[/color][/b]'
                return
            
            elif self.root.ids.support_name.text is None or len(self.root.ids.support_name.text) < 1:
                self.root.ids.support_error_msg.text = '[b][color=cc4444]Please enter a name.[/color][/b]'
                return
            
            elif self.root.ids.support_message.text is None or len(self.root.ids.support_message.text) < 1:
                self.root.ids.support_error_msg.text = '[b][color=cc4444]Please enter a message.[/color][/b]'
                return
            
            # Disable the send button for a while
            self.root.ids.support_error_msg.text = '[b][color=44cc44]Preparing...[/color][/b]'
            self.root.ids.support_send_button.set_off()
            Clock.schedule_once(self.on_support_button_enable, SUPPORT_ENABLE_TIMEOUT)
            
            support_ticket_data = {}
            support_ticket_data['source'] = 'API'
            support_ticket_data['autorespond'] = 'false'
            support_ticket_data['name'] = self.root.ids.support_name.text
            support_ticket_data['email'] = self.root.ids.support_email.text
            support_ticket_data['message'] = self.root.ids.support_message.text
            support_ticket_data['subject'] = 'Windows App v' + str(APP_VERSION)
            
            if self.root.ids.support_send_debug_info.active == True:
                debug_info = []          
                try:
                    self._debug_log.flush()
                    
                    debug_file = open(join(get_app_path(), DEBUG_REPORT_FILENAME), 'r')
                    debug_log_data = debug_file.read()
                    debug_file.close()
                    base64_debug_log = base64.b64encode(debug_log_data).decode('ascii')
                    crash_log = {DEBUG_REPORT_FILENAME: 'data:text/plain;base64,' + base64_debug_log}
                    debug_info.append(crash_log)
                except:
                    self.log(LOG_WARN, "No debug log found with filename: " + CRASH_REPORT_FILENAME)
                    e = sys.exc_info()[0]
                    stacktrace =  traceback.format_exc()
                    print stacktrace
                
                try:
                    crashreport_file = open(join(get_app_path(), CRASH_REPORT_FILENAME), 'r')
                    crash_log_data = crashreport_file.read()
                    crashreport_file.close()
                    base64_crash_log = base64.b64encode(crash_log_data).decode('ascii')
                    crash_log = {'crash-report.txt': 'data:text/plain;base64,' + base64_crash_log}
                    debug_info.append(crash_log)
                except:
                    self.log(LOG_WARN, "No crash report found with filename: " + CRASH_REPORT_FILENAME)
                    e = sys.exc_info()[0]
                    stacktrace =  traceback.format_exc()
                    print stacktrace

                stunnel_log_filename = ''
                # TODO: change the hardcoded filename
                stunnel_log_filename = r'{}\stunnel.log'.format(get_stunnel_dir())
                if not os.path.isfile(stunnel_log_filename):
                    self.log(LOG_WARN, r"stunnel\stunnel.log file not found within ProgramFiles")
                else:
                    try:
                        stunnel_log_file = open(stunnel_log_filename, 'r')
                        stunnel_log_text = stunnel_log_file.read()
                        stunnel_log_file.close()
                        base64_stunnel_log = base64.b64encode(stunnel_log_text).decode('ascii')
                        stunnel_log = {'stunnel-log.txt': 'data:text/plain;base64,' + base64_stunnel_log}
                        debug_info.append(stunnel_log)
                    except:
                        self.log(LOG_WARN, "Failed to load stunnel.log with filename: " + stunnel_log_filename)
                
                # add the debug info as attachments to the ticket
                support_ticket_data['attachments'] = debug_info
        
            # prepare support ticket
            self.root.ids.support_error_msg.text = '[b][color=44cc44]Sending...[/color][/b]'
            
            support_ticket_headers = {'Content-type': 'application/x-www-form-urlencoded',
                                      'Accept': 'text/plain',
                                      'X-API-Key': SUPPORT_API_KEY}

            support_request = UrlRequest(SUPPORT_URL, 
                                        on_success=self.on_support_ticket_posted, 
                                        on_failure=self.on_support_ticket_failure, 
                                        on_error=self.on_support_ticket_error, 
                                        req_body=json.dumps(support_ticket_data),
                                        req_headers=support_ticket_headers)
                                        
        # bye! see you in a callback: on_support_ticket_posted(), on_support_ticket_failure(), on_support_ticket_error()
    
    def on_support_ticket_failure(self, request, result):
        self.root.ids.support_error_msg.text = '[b][color=cc4444]Failure sending support ticket.[/color][/b]'
        
        self.log(LOG_ERROR, 'on_support_ticket_failure(): error: ' + str(request.error))
        self.log(LOG_ERROR, 'on_support_ticket_failure(): result: ' + str(request.result))
        
        
    def on_support_ticket_error(self, request, result):
        self.root.ids.support_error_msg.text = '[b][color=cc4444]Error sending support ticket.[/color][/b]'
        
        self.log(LOG_ERROR, 'on_support_ticket_error(): error: ' + str(request.error))
        self.log(LOG_ERROR, 'on_support_ticket_error(): result: ' + str(request.result))
        
        
    def on_support_ticket_posted(self, request, result):
        self.log(LOG_INFO, 'Ticket created: #' + str(request.result))
        self.root.ids.support_error_msg.text = '[b][color=44cc44]Support ticket #' + str(request.result) + ' created.[/color][/b]'
        self.root.ids.support_message.text = ''

        
    def on_support_button_enable(self, deltatime):
        self.root.ids.support_send_button.set_on()
        self.root.ids.support_error_msg.text = ''
        self.log(LOG_INFO, 'Re-enabled support send button')
    
    ########################################################
    #   Systray callbacks
    ########################################################

    def do_connect_vpn(self):
        self.on_vpn_switch_clicked()
    
    def do_disconnect_vpn(self):
        self.on_vpn_switch_clicked()
    

class MessageBox(WindowsApp):
    """
    Blocking popup.
    Source:
    https://stackoverflow.com/a/26876912/3477220
    """
    def __init__(self, parent, titleheader="Title", message="Message", options={"OK": ""}, size=(300, 300)):

        def popup_callback(instance):
            "callback for button press"
            self.retvalue = instance.text
            self.popup.dismiss()

        self.parent = parent
        self.retvalue = None
        self.titleheader = titleheader
        self.message = message
        self.options = options
        self.size = size
        box = GridLayout(orientation='vertical', cols=1)
        box.add_widget(Label(text=self.message, font_size=16))
        b_list = []
        buttonbox = BoxLayout(orientation='horizontal')
        for b in self.options:
            b_list.append(Button(text=b, size_hint=(1,.35), font_size=20))
            b_list[-1].bind(on_press=popup_callback)
            buttonbox.add_widget(b_list[-1])
        box.add_widget(buttonbox)
        self.popup = Popup(title=titleheader, content=box, size_hint=(None, None), size=self.size)
        self.popup.open()
        self.popup.bind(on_dismiss=self.on_close)

    def on_close(self, event):
        self.popup.unbind(on_dismiss=self.on_close)
        self.popup.dismiss()
        if self.retvalue != None and self.options[self.retvalue] != "" and self.options[self.retvalue] is not None:
            self.options[self.retvalue]()
