import os
import sys
import traceback

from Queue import Empty
from os.path import dirname, join, abspath

from kivy.config import Config
from kivy.logger import Logger, LoggerHistory

from vpncontrol import VpnControl
from systemtray import SystemTray
from globals import *
from funcs import get_app_path


def start_kivy_window():
    Config.set('graphics', 'window_state', 'visible')
    from windows import WindowsApp
   
    systray = SystemTray()
    vpncontrol = VpnControl()   

    debug_log = None
    logfile_path = join(get_app_path(), DEBUG_REPORT_FILENAME)
    try:
        # TODO use log lib to make simple log rotation, no need to invent a wheel
        max_log_size_bytes = 1*1024*1024
        # clear debug file if it's too large
        mode = 'w+' if os.path.isfile(logfile_path) and os.path.getsize(logfile_path) > max_log_size_bytes else 'a+'
        linebuffer = 1
        debug_log = open(logfile_path, mode, linebuffer)

        print "blackVPN: launching app"
        app = WindowsApp()
        app.set_system_tray(systray)
        app.set_vpn_control(vpncontrol)
        app.set_debug_log(debug_log)
        app.run()
        print "blackVPN: finished running app"

        if debug_log is not None and not debug_log.closed:
            debug_log.close()

        # If the user quits the app - restart the OpenVPNService
        # this will close any VPN connections left open
        # it will also reboot the service in case they are in a stuck state (say after too many AUTH_FAILs)
        #print "Restarting the openVPN service..."
        #os.system('net.exe stop "OpenVPNService"')
        #os.system('net.exe start "OpenVPNService"')
        #print "Service restarted!"

    except KeyboardInterrupt:
        raise
    except Empty:
        print "blackVPN:caught Twisted empty queue crash on app exit"
    except:
        print "Head shot. Doh!"
        
        e = sys.exc_info()[0]
        stacktrace = traceback.format_exc()
        print stacktrace
                
        # create a crash report
        app_path = get_app_path()
        crashreport_filename = join(app_path, CRASH_REPORT_FILENAME)
        crashreport_file = open(crashreport_filename, 'a')
        
        kivy_log = ''
        for record in reversed(LoggerHistory.history):
            kivy_log += record.msg + '\n'
                    
        crashreport_file.write(kivy_log)  # Kivy last 100 messages
        
        if debug_log is not None and not debug_log.closed:
            debug_log.close()
        # this is not working because after a crash the file has not been closed
        try:
            debug_filename = join(app_path, DEBUG_REPORT_FILENAME)
            debug_log = open(debug_filename, 'r')
            crashreport_file.write(debug_log.read())
        except:
            print "Cound not load debug log file: " + DEBUG_REPORT_FILENAME
        
        crashreport_file.write(str(e))
        crashreport_file.write(str(stacktrace))
        crashreport_file.close()    
    finally:
        if debug_log is not None and not debug_log.closed:
            debug_log.close()

        if systray is not None:
            systray.kill()


if __name__ == '__main__':
    start_kivy_window()

